variable "env_id" {}

variable "short_env_id" {}

variable "bucket_suffix" {}

variable "tags" {}

variable "access_key" {}

variable "secret_key" {}

variable "region" {}

variable "availability_zones" {
    type = "list"
}