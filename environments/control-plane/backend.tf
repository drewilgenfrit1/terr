terraform {
    backend "s3"{
        bucket = "pivotal-control-plane"
        key = "terraform.tfstate"
        region = "us-gov-west-1"
    }
}

module "main" {
    source = "../../PCFS-Control-Plane/aws/terraform"
    env_id = "${var.env_id}"
    short_env_id = "${var.short_env_id}"
    bucket_suffix = "${var.bucket_suffix}"
    tags = "${var.tags}"
    access_key = "${var.access_key}"
    secret_key = "${var.secret_key}"
    region = "${var.region}"
    availability_zones = "${var.availability_zones}"
}
