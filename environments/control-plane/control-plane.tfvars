env_id             = "control-plane"
short_env_id       = "cp"
region             = "us-gov-west-1"
availability_zones = ["us-gov-west-1a", "us-gov-west-1b", "us-gov-west-1c"]
bucket = "pivotal-mcboss-state"
bucket_key = "env/control-plane/terraform-cp-${timestamp()}.tfstate"
ops_manager_ami    = "ami-69b0d008"
dns_suffix         = "mcboss.net"
rds_instance_count = 0
ssh_key_name       = "control-plane"
vpc_cidr           = "10.64.0.0/20"
ops_manager_url    = "opsman.cp.pas.mcboss.net"
concourse_url      = "concourse.cp.pas.mcboss.net"
bucket_suffix      = "opsman"
vpn-z1-01_name     = "tso-cp-vpn-z1-01"

tso-cp-cidr = "10.64.10.0/27"
tso-cp-z1-perimeter-cidr = "10.64.10.32/27"
tso-cp-z1-private-cidr = "10.64.10.64/27"
tso-cp-z1-mgt-cidr = "10.64.10.96/27"
bosh_inbound_cidr = "0.0.0.0/0"

tso-cp-eni-vpn-z1-01-mgt-ip = "10.64.10.100"
tso-cp-eni-vpn-z1-01-private-ip = "10.64.10.70"
tso-cp-eni-vpn-z1-01-perimeter-ip = "10.64.10.40"
vdss-cp-mgt-ip = "10.0.8.121/32"
vdss-vpn-ip = "10.0.8.68/32"
pilot_vdss_pcx = "pcx-a87154c1"
tags               = {
    Team = "mcboss-control-plane"
    Project = "mcboss"
}