env_name           = "development-pas"
region             = "us-gov-west-1"
availability_zones = ["us-gov-west-1a", "us-gov-west-1b", "us-gov-west-1c"]
ops_manager_ami    = "ami-67c9ab06"
rds_instance_count = 0
dns_suffix         = "mcboss.net"
vpc_cidr           = "10.193.0.0/16"
ops_manager_vm     = true
internetless       = true
ops_manager_private = true
create_versioned_pas_buckets = true
create_backup_pas_buckets    = true
vpn-z1-01_name = "tso-development-vpn-z1-01"
tso-dev-cidr = "10.193.200.0/27"
tso-dev-z1-perimeter-cidr = "10.193.200.32/27"
tso-dev-z1-private-cidr = "10.193.200.64/27"
tso-dev-z1-mgt-cidr = "10.193.200.96/27"

tso-dev-eni-vpn-z1-01-mgt-ip = "10.193.200.100"
tso-dev-eni-vpn-z1-01-private-ip = "10.193.200.70"
tso-dev-eni-vpn-z1-01-perimeter-ip = "10.193.200.40"
vdss-mgt-ip = "10.0.8.121/32"
vdss-vpn-ip = "10.0.8.68/32"

pilot_vdss_pcx = "pcx-2f8ad846"