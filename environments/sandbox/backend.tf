terraform {
    backend "s3" {
        bucket = "pivotal-sandbox"
        key = "terraform.tfstate"
        region = "us-gov-west-1"
    }
}

module "main" {
    source = "../../terraforming-pas"
    env_name = "${var.env_name}"
    dns_suffix = "${var.dns_suffix}"
    access_key = "${var.access_key}"
    secret_key = "${var.secret_key}"
    region = "${var.region}"
    availability_zones = "${var.availability_zones}"
}
