# Ops Manager Control Plane

This repository contains scripts and terraform templates that can be used to
deploy Pivotal Operations Manager, which can then be used to deploy Concourse
and other control plane components.

It consists of:

- Terraform to pave the IaaS (control-plane network, Ops Manager VM, Concourse LB)
- Ops Manager director config YML (to be applied via `om`)
- A named cloud-config to be used for Concourse
- Operations files to bundle additional control plane components with Concourse

## Prerequisites

These scripts require several tools to be installed locally:

- [terraform](https://www.terraform.io/downloads.html)
- [om](https://github.com/pivotal-cf/om/releases)
- [jq](https://stedolan.github.io/jq/)

### om version > 0.41

`om` newer than 0.41 is required for this to work. Prior versions do not support
the `--vars-env` option which will cause the deployment to fail.

## Deployment Instructions

- [**GCP** Deployment Instructions](./gcp/README.md)
- [**Azure** Deployment Instructions](./azure/README.md)
- [**AWS** Deployment Instructions](./aws/README.md)
- [**vSphere** Deployment Instructions](./vsphere/README.md)

## Questions / Feedback

`#pcfsolutions` on Pivotal Slack is the right place to ask questions or provide feedback if it can't be captured as a GitHub issue.
