
if [ "${PROJECT_TYPE}" == "vsphere" ]; then
  source ./set-bosh-proxy-creds-vsphere.sh
else
  source ./set-bosh-proxy-creds.sh
fi

export CONCOURSE_URL="https://$(terraform output concourse_dns)"

# login to the BOSH director Credhub using info from bbl
echo "Connecting to Credhub on BOSH Director...."
credhub login

UAA_ADMIN_SECRET_PATH=$(credhub find -n uaa-admin | grep name | awk '{print $NF}')
UAA_ADMIN_SECRET=$(credhub get -n $UAA_ADMIN_SECRET_PATH | grep value | awk '{print $NF}')

UAA_URL=$(terraform output concourse_dns)
uaac target $UAA_URL:8443
uaac token client get admin -s $UAA_ADMIN_SECRET
