echo "It is no longer necessary to set BOSH proxy credentials with a separate "
echo "script specific to vSphere. From now on, you can simply run "
echo "source set-bosh-proxy-creds.sh"
echo
echo "Sourcing set-bosh-proxy-creds.sh for you now..."
echo

source ./set-bosh-proxy-creds.sh
