#!/bin/bash

set -e

export SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

source ./set-bosh-proxy-creds.sh

bosh -n update-config --type=cloud --name=concourse \
  -v internal_tag_name=$(terraform output internal_tag_name) \
  -v lb_target_pool=$(terraform output concourse_target_pool) \
  -v nat_tag=$(terraform output internal_tag_name) \
  $SCRIPT_DIR/concourse-cloud-config.yml

rm -f $OM_KEY
