variable "project_id" {
  type = "string"
}

variable "region" {
  type = "string"
}

variable "zone" {
  type = "string"
}

variable "env_id" {
  type    = "string"
  default = "control-plane"
}

variable "credentials" {
  type        = "string"
  description = "Path to GCP service account JSON file"
}

variable "ops_manager_username" {
  type    = "string"
  default = "admin"
}

variable "ops_manager_password" {
  description = "Password for administrator user. Generated if left blank."
  type        = "string"
  default     = ""
}

variable "ops_manager_decryption_phrase" {
  description = "Decryption Phrase for Ops Manager Authentication. Generated if left blank."
  type        = "string"
  default     = ""
}

variable "concourse_password" {
  description = "Password for Concourse admin user. Generated if left blank."
  type        = "string"
  default     = ""
}

variable "subnet_cidr" {
  type    = "string"
  default = "10.0.0.0/16"
}

variable "create_iam_service_account_members" {
  description = "If set to true, create an IAM Service Account project roles"
  default     = true
}

variable "create_blobstore_service_account_key" {
  description = "Create a scoped service account key for gcs storage access"
  default     = true
}

variable "opsman_image_url" {
  type        = "string"
  description = "Location of ops manager image on google cloud storage"
}

variable "opsman_machine_type" {
  type    = "string"
  default = "n1-standard-2"
}

variable "opsman_storage_bucket_count" {
  type        = "string"
  description = "Optional configuration of a Google Storage Bucket for BOSH's blobstore"
  default     = "1"
}

variable "dns_name" {
  type        = "string"
  description = "Base DNS name to use.  An entry will be created at opsman.<X> and concourse.<X>"
}

variable "nat_machine_type" {
  default = "n1-standard-1"
}
