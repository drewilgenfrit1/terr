project_id       = "" # GCP project ID
region           = "" # GCP region (ie us-west1)
zone             = "" # GCP zone (ie us-west1-a)
credentials      = "" # path to GCP JSON service account key
opsman_image_url = "" # Ops Manager filename (from the GCP YML in pivnet, ie: "ops-manager-us/pcf-gcp-2.3-build.170.tar.gz")
dns_name         = "" # Base DNS name (entries will be created for opsman.X and concourse.X)
