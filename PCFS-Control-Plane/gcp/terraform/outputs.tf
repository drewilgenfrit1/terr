output "project" {
  value = "${var.project_id}"
}

output "availability_zone_name" {
  value = "${var.zone}"
}

output "dns_servers" {
  value = "${cidrhost(var.subnet_cidr, 1)},8.8.8.8"
}

output "reserved_ip_ranges" {
  value = "${cidrhost(var.subnet_cidr, 1)}-${cidrhost(var.subnet_cidr, 9)}"
}

output "network" {
  value = "${google_compute_network.control-plane-network.name}"
}

output "subnetwork" {
  value = "${google_compute_subnetwork.control-plane-subnet.name}"
}

output "dns_managed_zone" {
  value = "${google_dns_managed_zone.control-plane-dns-zone.name}"
}

output "internal_cidr" {
  value = "${var.subnet_cidr}"
}

output "internal_gw" {
  value = "${google_compute_subnetwork.control-plane-subnet.gateway_address}"
}

output "internal_tag_name" {
  value = "${google_compute_firewall.internal.name}"
}

output "ops_manager_dns" {
  value = "${replace(google_dns_record_set.ops-manager-dns.name, "/\\.$/", "")}"
}

output "concourse_dns" {
  value = "${replace(google_dns_record_set.concourse-dns.name, "/\\.$/", "")}"
}

output "ops_manager_ip" {
  value = "${google_compute_address.ops-manager-ip.address}"
}

output "ops_manager_ssh_private_key" {
  sensitive = true
  value     = "${tls_private_key.ops-manager.private_key_pem}"
}

output "ops_manager_ssh_public_key" {
  sensitive = true
  value     = "${format("ubuntu:%s", tls_private_key.ops-manager.public_key_openssh)}"
}

output "ops_manager_username" {
  value = "${var.ops_manager_username}"
}

output "ops_manager_password" {
  value     = "${var.ops_manager_password == "" ? random_id.ops_manager_password_generator.b64 : var.ops_manager_password}"
  sensitive = true
}

output "concourse_password" {
  value     = "${var.concourse_password == "" ? random_id.concourse_password_generator.b64 : var.concourse_password}"
  sensitive = true
}

output "ops_manager_decryption_phrase" {
  value     = "${var.ops_manager_decryption_phrase == "" ? random_id.ops_manager_decryption_phrase_generator.b64 : var.ops_manager_decryption_phrase}"
  sensitive = true
}

output "service_account_email" {
  value = "${google_service_account.opsman_service_account.email}"
}

output "region" {
  value = "${var.region}"
}

output "concourse_target_pool" {
  value = "${google_compute_target_pool.target-pool.name}"
}

output "concourse_lb_ip" {
  value = "${google_compute_address.concourse-address.address}"
}

output "director_storage_bucket" {
  value = "${google_storage_bucket.director.name}"
}

output "blobstore_service_account_key_base64" {
 value     = "${google_service_account_key.blobstore_service_account_key.private_key}"
 sensitive = true
}

output "ops_manager_service_account_key_base64" {
  value     = "${google_service_account_key.opsman_service_account_key.private_key}"
  sensitive = true
}
