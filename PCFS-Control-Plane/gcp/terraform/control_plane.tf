provider "google" {
  credentials = "${file("${var.credentials}")}"
  project     = "${var.project_id}"
  region      = "${var.region}"
}

resource "random_id" "ops_manager_password_generator" {
  byte_length = 16
}

resource "random_id" "ops_manager_decryption_phrase_generator" {
  byte_length = 16
}

resource "random_id" "concourse_password_generator" {
  byte_length = 16
}

resource "google_compute_network" "control-plane-network" {
  name                    = "${var.env_id}-network"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "control-plane-subnet" {
  name          = "${var.env_id}-subnet"
  ip_cidr_range = "${var.subnet_cidr}"
  network       = "${google_compute_network.control-plane-network.self_link}"
}

resource "google_compute_firewall" "external" {
  name    = "${var.env_id}-external"
  network = "${google_compute_network.control-plane-network.name}"

  source_ranges = ["0.0.0.0/0"]

  allow {
    ports    = ["22", "6868", "25555"]
    protocol = "tcp"
  }

  target_tags = ["${var.env_id}-bosh-open"]
}

resource "google_compute_firewall" "internal" {
  name    = "${var.env_id}-internal"
  network = "${google_compute_network.control-plane-network.name}"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
  }

  allow {
    protocol = "udp"
  }

  source_ranges = [
    "${google_compute_subnetwork.control-plane-subnet.ip_cidr_range}"
  ]
}

resource "google_compute_firewall" "firewall-concourse" {
  name    = "${var.env_id}-concourse-open"
  network = "${google_compute_network.control-plane-network.name}"

  allow {
    protocol = "tcp"
    ports    = ["80", "443", "2222", "8443", "8844"]
  }

  target_tags = ["concourse"]
}

resource "google_compute_address" "concourse-address" {
  name = "${var.env_id}-concourse"
}

resource "google_compute_target_pool" "target-pool" {
  name = "${var.env_id}-concourse"

  session_affinity = "NONE"
}

resource "google_compute_forwarding_rule" "ssh-forwarding-rule" {
  name        = "${var.env_id}-concourse-ssh"
  target      = "${google_compute_target_pool.target-pool.self_link}"
  port_range  = "2222"
  ip_protocol = "TCP"
  ip_address  = "${google_compute_address.concourse-address.address}"
}

resource "google_compute_forwarding_rule" "https-forwarding-rule" {
  name        = "${var.env_id}-concourse-https"
  target      = "${google_compute_target_pool.target-pool.self_link}"
  port_range  = "443"
  ip_protocol = "TCP"
  ip_address  = "${google_compute_address.concourse-address.address}"
}

resource "google_compute_forwarding_rule" "http-forwarding-rule" {
  name        = "${var.env_id}-concourse-http"
  target      = "${google_compute_target_pool.target-pool.self_link}"
  port_range  = "80"
  ip_protocol = "TCP"
  ip_address  = "${google_compute_address.concourse-address.address}"
}

resource "google_compute_forwarding_rule" "credhub-forwarding-rule" {
  name        = "${var.env_id}-concourse-credhub"
  target      = "${google_compute_target_pool.target-pool.self_link}"
  port_range  = "8844"
  ip_protocol = "TCP"
  ip_address  = "${google_compute_address.concourse-address.address}"
}

resource "google_compute_forwarding_rule" "uaa-forwarding-rule" {
  name        = "${var.env_id}-concourse-uaa"
  target      = "${google_compute_target_pool.target-pool.self_link}"
  port_range  = "8443"
  ip_protocol = "TCP"
  ip_address  = "${google_compute_address.concourse-address.address}"
}

resource "google_service_account" "opsman_service_account" {
  account_id   = "${var.env_id}-opsman"
  display_name = "${var.env_id} Ops Manager VM Service Account"
}

resource "google_service_account_key" "opsman_service_account_key" {
  service_account_id = "${google_service_account.opsman_service_account.id}"
}

resource "google_project_iam_member" "opsman_iam_service_account_actor" {
  count   = "${var.create_iam_service_account_members}"
  project = "${var.project_id}"
  role    = "roles/iam.serviceAccountActor"
  member  = "serviceAccount:${google_service_account.opsman_service_account.email}"
}

resource "google_project_iam_member" "opsman_compute_instance_admin" {
  count   = "${var.create_iam_service_account_members}"
  project = "${var.project_id}"
  role    = "roles/compute.instanceAdmin"
  member  = "serviceAccount:${google_service_account.opsman_service_account.email}"
}

resource "google_project_iam_member" "opsman_compute_network_admin" {
  count   = "${var.create_iam_service_account_members}"
  project = "${var.project_id}"
  role    = "roles/compute.networkAdmin"
  member  = "serviceAccount:${google_service_account.opsman_service_account.email}"
}

resource "google_project_iam_member" "opsman_compute_storage_admin" {
  count   = "${var.create_iam_service_account_members}"
  project = "${var.project_id}"
  role    = "roles/compute.storageAdmin"
  member  = "serviceAccount:${google_service_account.opsman_service_account.email}"
}

resource "google_project_iam_member" "opsman_storage_admin" {
  count   = "${var.create_iam_service_account_members}"
  project = "${var.project_id}"
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.opsman_service_account.email}"
}

resource "google_service_account" "blobstore_service_account" {
  account_id   = "${var.env_id}-blobstore"
  display_name = "${var.env_id} Blobstore Service Account"
}

resource "google_service_account_key" "blobstore_service_account_key" {
  service_account_id = "${google_service_account.blobstore_service_account.id}"
}

data "google_service_account_key" "blobstore_service_account_key" {
  name = "${google_service_account_key.blobstore_service_account_key.name}"
}

resource "google_project_iam_member" "blobstore_cloud_storage_admin" {
  count   = "${var.create_blobstore_service_account_key}"
  project = "${var.project_id}"
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.blobstore_service_account.email}"
}

# Allow HTTP/S access to Ops Manager from the outside world
resource "google_compute_firewall" "ops-manager-external" {
  name        = "${var.env_id}-ops-manager-external"
  network     = "${google_compute_network.control-plane-network.name}"
  target_tags = ["${var.env_id}-ops-manager-external"]

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "443"]
  }
}

resource "google_compute_image" "ops-manager-image" {
  name = "${var.env_id}-ops-manager-image"

  timeouts {
    create = "20m"
  }

  raw_disk {
    source = "https://storage.googleapis.com/ops-manager-${replace(var.opsman_image_url, "/.*ops-manager-(.*)/", "$1")}"
  }
}

resource "google_compute_address" "ops-manager-ip" {
  name = "${var.env_id}-ops-manager-ip"
}

resource "google_compute_instance" "ops-manager" {
  name         = "${var.env_id}-ops-manager"
  machine_type = "${var.opsman_machine_type}"
  zone         = "${var.zone}"
  tags         = ["${var.env_id}-ops-manager-external", "${var.env_id}-bosh-open"]

  timeouts {
    create = "10m"
  }

  boot_disk {
    initialize_params {
      image = "${google_compute_image.ops-manager-image.self_link}"
      size  = 150
    }
  }

  network_interface {
    subnetwork = "${google_compute_subnetwork.control-plane-subnet.name}"

    access_config {
      nat_ip = "${google_compute_address.ops-manager-ip.address}"
    }
  }

  service_account {
    email  = "${google_service_account.opsman_service_account.email}"
    scopes = ["cloud-platform"]
  }

  metadata = {
    ssh-keys               = "${format("ubuntu:%s", tls_private_key.ops-manager.public_key_openssh)}"
    block-project-ssh-keys = "TRUE"
  }
}

resource "google_storage_bucket" "director" {
  name          = "${var.project_id}-${var.env_id}-director"
  force_destroy = true
}

resource "tls_private_key" "ops-manager" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

resource "google_dns_managed_zone" "control-plane-dns-zone" {
  name        = "${var.env_id}-zone"
  dns_name    = "${var.dns_name}."
  description = "DNS zone for the ${var.env_id} environment"
}

resource "google_dns_record_set" "ops-manager-dns" {
  name = "opsman.${google_dns_managed_zone.control-plane-dns-zone.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = "${google_dns_managed_zone.control-plane-dns-zone.name}"

  rrdatas = ["${google_compute_address.ops-manager-ip.address}"]
}

resource "google_dns_record_set" "concourse-dns" {
  name = "concourse.${google_dns_managed_zone.control-plane-dns-zone.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = "${google_dns_managed_zone.control-plane-dns-zone.name}"

  rrdatas = ["${google_compute_address.concourse-address.address}"]
}

resource "google_compute_instance" "nat" {
  name           = "${var.env_id}-nat"
  machine_type   = "${var.nat_machine_type}"
  zone           = "${var.zone}"
  create_timeout = 10
  tags           = ["${var.env_id}-nat-external"]
  count          = "1"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
      size  = 10
    }
  }

  network_interface {
    subnetwork = "${google_compute_subnetwork.control-plane-subnet.name}"
    address    = "${cidrhost(var.subnet_cidr, 3)}"

    access_config {
      # ephemeral IP
    }
  }

  can_ip_forward = true

  metadata_startup_script = <<EOF
#!/bin/bash
sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sed -i= 's/^[# ]*net.ipv4.ip_forward=[[:digit:]]/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
EOF
}

resource "google_compute_route" "nat-primary" {
  // Explicitly declare dependency on all of the .nat instances.
  // This is needed because we only explicitly reference the first nat instance.
  // We implicitly depend on the others because of the name interpolation.
  depends_on = ["google_compute_instance.nat"]

  name                   = "${var.env_id}-nat-primary"
  dest_range             = "0.0.0.0/0"
  network                = "${google_compute_network.control-plane-network.name}"
  next_hop_instance      = "${var.env_id}-nat"
  next_hop_instance_zone = "${var.zone}"
  priority               = 1000
  tags                   = ["${var.env_id}-internal"]
  count                  = "1"
}
