#!/bin/bash

set -e

export SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

export director_project_id=$(terraform output project)
export director_network_name="$(terraform output network)/$(terraform output subnetwork)/$(terraform output region)"
export director_gcp_service_account=$(terraform output service_account_email)
export director_internal_cidr=$(terraform output internal_cidr)
export director_internal_gw=$(terraform output internal_gw)
export director_dns_servers=$(terraform output dns_servers)
export director_reserved_ip_ranges=$(terraform output reserved_ip_ranges)
export director_availability_zone=$(terraform output availability_zone_name)
export director_internal_tag_name=$(terraform output internal_tag_name)
export director_blobstore_bucket=$(terraform output director_storage_bucket)

source ./set-om-creds.sh

echo "Configuring Ops Manager Authentication"
om -t $OM_TARGET --skip-ssl-validation \
  configure-authentication \
    --decryption-passphrase $(terraform output ops_manager_decryption_phrase) \
    --username $(terraform output ops_manager_username) \
    --password $(terraform output ops_manager_password)

echo "Configuring Ops Manager Director"
echo "blobstore_service_account: '$(terraform output blobstore_service_account_key_base64 | base64 -D | jq -c .)'" > service_account_vars.yml
trap "rm -f service_account_vars.yml" EXIT
om -t $OM_TARGET --skip-ssl-validation \
  configure-director --config $SCRIPT_DIR/director.yml --vars-env=director --vars-file=service_account_vars.yml

echo "Deploying Ops Manager Director"
om -t $OM_TARGET --skip-ssl-validation apply-changes
