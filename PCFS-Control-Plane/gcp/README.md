# Ops Manager Control Plane for GCP

This directory contains all the GCP specific bits to deploy Concourse
and other control plane components.

### GCP Service Account for Terraform

Terraform requires a service account in order to bootstrap the OpsMan VM and other resources. This is not the service account used by OpsMan - terraform will create that as well. The simplest and most future proof setup for this account is to grant it **Project Owner** (Project Editor is not enough). A more minimal set of permissions follows:

* Compute Instance Admin (beta) - for creating OM VM
* Compute Network Admin - for creating networks
* Compute Security Admin - for creating firewalls
* Compute Storage Admin - for creating images
* DNS Administrator - for creating the DNS zone
* Service Account Admin - for creating service accounts
* Service Account Key Admin - for creating service account keys
* Service Account User - for acting as a service account
* Project IAM Admin - for assiging roles to service accounts
* Storage Admin - for creating the GCS buckets

## Deployment

**Note:** All commands listed here should be run from the root directory of this repository.

### Configure Terraform Variables

Start by making a copy of the terraform vars template for your IaaS and filling it out.

For example, on GCP:

```
$ cp gcp/terraform/example.tfvars gcp/terraform/control-plane.tfvars
```

### Pave Your IaaS

This creates a single network and deploys an Ops Manager VM to it.
It also creates a load balancer for use with Concourse.

```
$ terraform init ./gcp/terraform
$ terraform apply -var-file=./gcp/terraform/control-plane.tfvars ./gcp/terraform
```

### Configure OpsMan and Deploy Director

Configure Ops Manager and deploy the director using the script in the directory for your IaaS:

```
$ ./gcp/deploy-om-director.sh
```


### Deploy Concourse

Add a named cloud-config for the Concourse deployment:

```
$ ./gcp/update-concourse-cloud-config.sh
```

Upload the Xenial stemcell required by Concourse:

Check [bosh.io](https://bosh.io/stemcells/bosh-google-kvm-ubuntu-xenial-go_agent)
for the latest **Google Xenial Full stemcell**.

Make sure you are in a **bash shell** before running the source command, it will not work in zsh.
```
$ source ./set-bosh-proxy-creds.sh
$ bosh upload-stemcell --sha1 3bc256c5b10a170be1a22ad97f7d511733b53538 \
   https://bosh.io/d/stemcells/bosh-google-kvm-ubuntu-xenial-go_agent?v=97.22
```

Finally deploy concourse:

You will need to clone [concourse-bosh-deployment](https://github.com/concourse/concourse-bosh-deployment) first.
Then before running the deploy script below, set **CONCOURSE_BOSH_DEPLOYMENT** to the path of your checkout.

```
$ cd ~/code
$ git clone https://github.com/concourse/concourse-bosh-deployment
$ export CONCOURSE_BOSH_DEPLOYMENT=~/code/concourse-bosh-deployment
$ ./deploy-concourse.sh
```

## Tear Down

### Delete all Resources not managed by Terraform

There are a number of options to do this. If Concourse has been deployed,
running `bosh delete-deployment -d concourse` will clean these VMs up.
You will also need to delete the stemcells and disks with `bosh clean-up --all`.

You will also need to delete the bosh director VM from the Google Cloud Console or by using `gcloud`.

Alternatively you will just need to delete everything (VMs, disks, images) from the Google Cloud console,
but **do not delete** the OpsMan VM or the Control Plane NAT VM as this will cause issues later with terraform.

### Cleanup Terraform Resources

Use terraform destroy to cleanup resources created by Terraform including VMs,
images, networks, roles, firewalls, etc:

```
terraform destroy -var-file=./gcp/terraform/control-plane.tfvars ./gcp/terraform
```

If terraform destroy fails you may still have VMs running that are not managed
by terraform and will need to delete them by hand.
