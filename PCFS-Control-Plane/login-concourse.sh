#!/bin/bash

if [ "${PROJECT_TYPE}" == "vsphere" ]; then
  source ./set-bosh-proxy-creds-vsphere.sh
else
  source ./set-bosh-proxy-creds.sh
fi

CONCOURSE_PASSWORD="$(terraform output concourse_password)"
export CONCOURSE_URL="https://$(terraform output concourse_dns)"

fly -t concourse login -c $CONCOURSE_URL -u admin -p $CONCOURSE_PASSWORD -k
