# (required) Azure location to stand up environment in
location = "WestUS2"

# (required) Azure account subscription id
subscription_id = "some-guid"

# (required) Azure account tenant id
tenant_id = "some-other-guid"

# (required) Azure automation account client id
client_id = "some-other-other-guid"

# (required) Azure automation account client secret
client_secret = "superdupersecret"

# (required) Domain to add environment subdomain to
dns_suffix = "example.com"

# (optional) if left blank, will default to "pcf".
dns_subdomain = "pcf"

# An arbitrary unique name for namespacing resources, defaults to controlplane
env_name = "controlplane"

# (required) URL for an OpsMan image hosted on Azure
ops_manager_image_uri = "https://opsmanagerwestus.blob.core.windows.net/images/ops-manager-2.3-build.188.vhd"
