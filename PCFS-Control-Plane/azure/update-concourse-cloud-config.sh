#!/bin/bash

set -e

export SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

source ./set-bosh-proxy-creds.sh

bosh -n update-config --type=cloud --name=concourse \
  -v concourse_lb_name=$(terraform output concourse_lb_name) \
  $SCRIPT_DIR/concourse-cloud-config.yml

rm -f $OM_KEY
