# Ops Manager Control Plane for Azure

This directory contains all the Azure specific bits to deploy Concourse
and other control plane components.

## Deployment

**Note:** All commands listed here should be run from the root directory of this repository.

### Azure Service Account for Terraform

Terraform requires a service principal in order to bootstrap the control plane OpsMan VM and other resources. This repository includes a `create-service-account.sh` script to make this multi-step process easier.

```
$ ./azure/create-service-account.sh <your-subscription-id> <your-tenant-id> <some-identifier-uri> "Service Principal for Control Plane"
```

**Note:** If you're wondering where you can find your subscription_id and tenant_id then run `az account list`. Use the id and tenantId respectively.

### Configure Terraform Variables

Start by making a copy of the terraform vars template for your IaaS and filling it out. For example, on Azure:

```
$ cp azure/terraform/example.tfvars azure/terraform/control-plane.tfvars
```

### Pave Your IaaS

This creates a single network and deploys an Ops Manager VM to it.
It also creates a load balancer for use with Concourse.

```
$ terraform init ./azure/terraform
$ terraform apply -var-file=./azure/terraform/control-plane.tfvars ./azure/terraform
```

### Configure OpsMan and Deploy Director

Configure Ops Manager and deploy the director using the script in the directory for your IaaS:

```
$ ./azure/deploy-om-director.sh
```


### Deploy Concourse

Add a named cloud-config for the Concourse deployment:

```
$ ./azure/update-concourse-cloud-config.sh
```

Upload the Xenial stemcell required by Concourse:

Check [bosh.io](https://bosh.io/stemcells/bosh-azure-hyperv-ubuntu-xenial-go_agent)
for the latest **Azure Xenial Full stemcell**.

Make sure you are in a **bash shell** before running the source command, it will not work in zsh.
```
$ source ./set-bosh-proxy-creds.sh
$ bosh upload-stemcell --sha1 2fb97fe3a297fe86329a752817028f2a08bc93e8 \
  https://bosh.io/d/stemcells/bosh-azure-hyperv-ubuntu-xenial-go_agent?v=170.2
```

Finally deploy concourse:

You will need to clone [concourse-bosh-deployment](https://github.com/concourse/concourse-bosh-deployment) first.
Then before running the deploy script below, set **CONCOURSE_BOSH_DEPLOYMENT** to the path of your checkout.

```
$ cd ~/code
$ git clone https://github.com/concourse/concourse-bosh-deployment
$ export CONCOURSE_BOSH_DEPLOYMENT=~/code/concourse-bosh-deployment
$ ./deploy-concourse.sh
```

### Access Credhub
Make sure you are in a **bash shell** before running the source command, it will not work in zsh.
```
$ ./target-concourse-credhub.sh
```
Run any credhub commands from the `/control-plane` directory to access the concourse credhub

### Access UAA
Make sure you are in a **bash shell** before running the source command, it will not work in zsh.
```
$ ./target-uaa.sh
```
Run any uaa cli commands from the `/control-plane` directory to access the concourse credhub

## Tear Down

### Delete all Resources not managed by Terraform

There are a number of options to do this. If Concourse has been deployed,
running `bosh delete-deployment -d concourse` will clean these VMs up.
You will also need to delete the stemcells and disks with `bosh clean-up --all`.

You will also need to delete the bosh director VM from the Google Cloud Console or by using `gcloud`.

Alternatively you will just need to delete everything (VMs, disks, images) from the Google Cloud console,
but **do not delete** the OpsMan VM or the Control Plane NAT VM as this will cause issues later with terraform.

### Cleanup Terraform Resources

Use terraform destroy to cleanup resources created by Terraform including VMs,
images, networks, roles, firewalls, etc:

```
terraform destroy -var-file=./azure/terraform/control-plane.tfvars ./azure/terraform
```

If terraform destroy fails you may still have VMs running that are not managed
by terraform and will need to delete them by hand.
