#!/bin/bash
#
# see https://github.com/cloudfoundry/bosh-azure-cpi-release/blob/master/docs/get-started/create-service-principal.sh
# for a more "official" azure version of this script.
set -eo pipefail

PASSWORD=$(openssl rand -base64 32)
SUBSCRIPTION_ID=$1
TENANT_ID=$2
UNIQUE_ID_URI=$3
DISPLAY_NAME=${4:-"Service Principal for BOSH"}

if [[ -z "$SUBSCRIPTION_ID" ]]; then
	echo "Enter subscription id:"
	read -r SUBSCRIPTION_ID
fi

if [[ -z "$TENANT_ID" ]]; then
	echo "Enter tenant id:"
	read -r TENANT_ID
fi

if [[ -z "$UNIQUE_ID_URI" ]]; then
	echo "Enter identifier uri:"
	read -r UNIQUE_ID_URI
fi

echo "Creating app ${DISPLAY_NAME}"
APP_ID=$(az ad app create --display-name "$DISPLAY_NAME" \
  --password "$PASSWORD" --homepage "$UNIQUE_ID_URI" \
  --identifier-uris "$UNIQUE_ID_URI" | jq -r .appId)

ASSIGNEE_ID=$(az ad sp create --id "$APP_ID" | jq -r .objectId)

# eventual consistency... retry until it works
until az role assignment create --assignee "$ASSIGNEE_ID" \
  --role "Contributor" --scope /subscriptions/"$SUBSCRIPTION_ID" > /dev/null 2>&1;
do
	echo 'Retrying role assignment...'
	sleep 1
done

echo "Logging in as ${APP_ID}..."
az login --username "$APP_ID" --password "$PASSWORD" \
  --service-principal --tenant "$TENANT_ID" > /dev/null 2>&1;

echo
echo "Done!"
echo
echo "subscription id: ${SUBSCRIPTION_ID}"
echo "tentant id: ${TENANT_ID}"
echo "client id: ${APP_ID}"
echo "client secret: ${PASSWORD}"
echo
