#!/bin/bash

set -e

export SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

export director_bosh_root_storage_account=$(terraform output bosh_root_storage_account)
export director_client_id=$(terraform output client_id)
export director_client_secret=$(terraform output client_secret)
export director_default_security_group_name=$(terraform output default_security_group_name)
export director_resource_group_name=$(terraform output resource_group_name)
export director_ops_manager_ssh_public_key=$(terraform output ops_manager_ssh_public_key)
export director_ops_manager_ssh_private_key=$(terraform output ops_manager_ssh_private_key)
export director_subscription_id=$(terraform output subscription_id)
export director_tenant_id=$(terraform output tenant_id)
export director_network=$(terraform output network)
export director_subnetwork=$(terraform output subnetwork)
export director_internal_cidr=$(terraform output internal_cidr)
export director_reserved_ip_ranges=$(terraform output reserved_ip_ranges)
export director_internal_gw=$(terraform output internal_gw)
export director_dns_servers=$(terraform output dns_servers)

source ./set-om-creds.sh

echo "Configuring Ops Manager Authentication"
om -t $OM_TARGET --skip-ssl-validation \
  configure-authentication \
    --decryption-passphrase $(terraform output ops_manager_decryption_phrase) \
    --username $(terraform output ops_manager_username) \
    --password $(terraform output ops_manager_password)

echo "Configuring Ops Manager Director"
om -t $OM_TARGET --skip-ssl-validation \
  configure-director --config $SCRIPT_DIR/director.yml --vars-env=director

echo "Deploying Ops Manager Director"
om -t $OM_TARGET --skip-ssl-validation apply-changes
