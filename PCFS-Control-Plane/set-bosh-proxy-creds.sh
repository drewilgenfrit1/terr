source ./set-om-creds.sh

if [ "$PROJECT_TYPE" != 'vsphere' ]; then
  export OM_KEY=om.pem
  terraform output ops_manager_ssh_private_key > $OM_KEY
  chmod 0600 $OM_KEY
else
  unset OM_KEY
fi

CREDS=$(om -t $OM_TARGET --skip-ssl-validation curl --silent \
     -p /api/v0/deployed/director/credentials/bosh_commandline_credentials | \
  jq -r .credential | sed 's/bosh //g')

# this will set BOSH_CLIENT, BOSH_ENVIRONMENT, BOSH_CLIENT_SECRET, and BOSH_CA_CERT
# however, BOSH_CA_CERT will be a path that is only valid on the OM VM
array=($CREDS)
for VAR in ${array[@]}; do
  export $VAR
done

export BOSH_CA_CERT="$(om -t $OM_TARGET --skip-ssl-validation certificate-authorities -f json | \
    jq -r '.[] | select(.active==true) | .cert_pem')"

if [ "$PROJECT_TYPE" != 'vsphere' ]; then
  export BOSH_ALL_PROXY="ssh+socks5://ubuntu@$OM_TARGET:22?private-key=$OM_KEY"
fi

if [ "$PROJECT_TYPE" != 'vsphere' ]; then
  export CREDHUB_PROXY=$BOSH_ALL_PROXY
fi
export CREDHUB_CLIENT=$BOSH_CLIENT
export CREDHUB_SECRET=$BOSH_CLIENT_SECRET
export CREDHUB_CA_CERT=$BOSH_CA_CERT
export CREDHUB_SERVER="https://$BOSH_ENVIRONMENT:8844"
