#!/bin/bash

if [ "${PROJECT_TYPE}" == "vsphere" ]; then
  source ./set-bosh-proxy-creds-vsphere.sh
else
  source ./set-bosh-proxy-creds.sh
fi

# local path to https://github.com/concourse/concourse-bosh-deployment
if [ -z ${CONCOURSE_BOSH_DEPLOYMENT} ]; then
  CONCOURSE_BOSH_DEPLOYMENT=~/concourse-bosh-deployment
fi

export DB_PERSISTENT_DISK_TYPE=102400

export CONCOURSE_HOST=$(terraform output concourse_dns)
export CONCOURSE_PASSWORD=$(terraform output concourse_password)


bosh -n deploy -d concourse $CONCOURSE_BOSH_DEPLOYMENT/cluster/concourse.yml \
   -l $CONCOURSE_BOSH_DEPLOYMENT/versions.yml \
   -l versions.yml \
   -v deployment_name=concourse \
   -v network_name=control-plane \
   -v web_network_name=control-plane \
   -v web_network_vm_extension=concourse-lb \
   -v external_host=${CONCOURSE_HOST} \
   -v external_url="https://${CONCOURSE_HOST}" \
   -v web_vm_type="concourse.web" \
   -v db_vm_type="concourse.db" \
   -v worker_vm_type="concourse.worker" \
   -v db_persistent_disk_type=$DB_PERSISTENT_DISK_TYPE \
   -v local_user.username=admin \
   -v local_user.password=$CONCOURSE_PASSWORD \
   -o $CONCOURSE_BOSH_DEPLOYMENT/cluster/operations/basic-auth.yml \
   -o $CONCOURSE_BOSH_DEPLOYMENT/cluster/operations/privileged-http.yml \
   -o $CONCOURSE_BOSH_DEPLOYMENT/cluster/operations/privileged-https.yml \
   -o $CONCOURSE_BOSH_DEPLOYMENT/cluster/operations/tls.yml \
   -o $CONCOURSE_BOSH_DEPLOYMENT/cluster/operations/tls-vars.yml \
   -o $CONCOURSE_BOSH_DEPLOYMENT/cluster/operations/web-network-extension.yml \
   -o concourse/worker-vm-extension.yml \
   -o concourse/update-azs.yml \
   -v availability_zone=$(terraform output availability_zone_name) \
   -o concourse/add-credhub-uaa-to-web.yml
   # -o operations/enable-db-backups.yml \
   # -o operations/backup-atc-db.yml \
   # -o operations/backup-uaa-db.yml \
   # -o operations/backup-credhub-db.yml
