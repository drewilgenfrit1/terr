# VPN-Z1-01 ENI

resource "aws_network_interface" "tso-cp-eni-vpn-z1-01-perimeter" {
  subnet_id       = "${aws_subnet.tso-cp-subnet-z1-perimeter.id}"
  private_ips     = ["${var.tso-cp-eni-vpn-z1-01-perimeter-ip}"]
  security_groups = ["${aws_security_group.tso-cp-vpn-sg.id}"]

  source_dest_check = false

  attachment {
    instance     = "${aws_instance.tso-cp-vpn-z1-01.id}"
    device_index = 1
  }
}

resource "aws_network_interface" "tso-cp-eni-vpn-z1-01-private" {
  subnet_id         = "${aws_subnet.tso-cp-subnet-z1-private.id}"
  private_ips       = ["${var.tso-cp-eni-vpn-z1-01-private-ip}"]
  security_groups   = ["${aws_security_group.tso-cp-private-sg.id}"]
  source_dest_check = false

  attachment {
    instance     = "${aws_instance.tso-cp-vpn-z1-01.id}"
    device_index = 2
  }
}

###
### End of MCBOSS-Provided code
###
