# VPN-Z1-01

resource "aws_instance" "tso-cp-vpn-z1-01" {
  ami               = "${var.vsrx_default_ami}"
  availability_zone = "${var.availability_zones[0]}"
  instance_type     = "c4.xlarge"
  key_name          = "${aws_key_pair.tso_cp.key_name}"

  monitoring = true

  root_block_device {
    volume_type = "gp2"
  }

  ebs_optimized = false #Not available for t2 instances

  private_ip = "${var.tso-cp-eni-vpn-z1-01-mgt-ip}"
  subnet_id  = "${aws_subnet.tso-cp-subnet-z1-mgt.id}"

  vpc_security_group_ids = ["${aws_security_group.tso-cp-mgt-sg.id}"]

  tags {
    Name = "${var.vpn-z1-01_name}"
  }
}

###
### End of MCBOSS-Provided code
###
