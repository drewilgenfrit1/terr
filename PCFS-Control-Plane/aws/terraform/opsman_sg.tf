resource "aws_security_group" "control_plane_ops_manager" {
  name        = "${var.env_id}-control-plane-ops-manager-security-group"
  description = "Control Plane Opsman SG"
  vpc_id      = "${local.vpc_id}"

  tags {
    Name = "${var.env_id}-control-plane-opsman-security-group"
  }

  lifecycle {
    ignore_changes = ["name", "description"]
  }
}

resource "aws_security_group_rule" "control_plane_ops_manager_ssh" {
  security_group_id = "${aws_security_group.control_plane_ops_manager.id}"
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 22
  to_port           = 22
  source_security_group_id = "${aws_security_group.tso-cp-private-sg.id}"
  #cidr_blocks       = ["${var.bosh_inbound_cidr}"]
}

resource "aws_security_group_rule" "control_plane_ops_manager_http" {
  security_group_id = "${aws_security_group.control_plane_ops_manager.id}"
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 80
  to_port           = 80
  source_security_group_id = "${aws_security_group.tso-cp-private-sg.id}"
  #cidr_blocks       = ["${var.bosh_inbound_cidr}"]
}

resource "aws_security_group_rule" "control_plane_ops_manager_https" {
  security_group_id = "${aws_security_group.control_plane_ops_manager.id}"
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 443
  to_port           = 443
  source_security_group_id = "${aws_security_group.tso-cp-private-sg.id}"
  #cidr_blocks       = ["${var.bosh_inbound_cidr}"]
}

resource "aws_security_group_rule" "control_plane_ops_manager_egress" {
  security_group_id = "${aws_security_group.control_plane_ops_manager.id}"
  type              = "egress"
  protocol          = "-1"
  from_port         = 0
  to_port           = 0
  cidr_blocks       = ["0.0.0.0/0"]
}