# PILOT MGT ENI security group

resource "aws_security_group" "tso-cp-mgt-sg" {
  name        = "tso-cp-mgt-sg"
  description = "ports and protocols for SMTP servers"
  vpc_id      = "${local.vpc_id}"

  tags {
    Name = "tso-cp-mgt-sg"
  }
}

# PILOT VPN ENI security group

resource "aws_security_group" "tso-cp-vpn-sg" {
  name        = "tso-cp-vpn-sg"
  description = "ports and protocols for VPN servers"
  vpc_id      = "${local.vpc_id}"

  tags {
    Name = "tso-cp-vpn-sg"
  }
}

# PILOT Private ENI security group

resource "aws_security_group" "tso-cp-private-sg" {
  name        = "tso-cp-private-sg"
  description = "ports and protocols for Private ENIs"
  vpc_id      = "${local.vpc_id}"

  tags {
    Name = "tso-cp-private-sg"
  }
}

# PILOT MGT ENI security group rules

resource "aws_security_group_rule" "sg-tso-cp-mgt-ingress-ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  security_group_id = "${aws_security_group.tso-cp-mgt-sg.id}"
  cidr_blocks       = ["${var.vdss-mgt-ip}"]
}

resource "aws_security_group_rule" "sg-tso-cp-mgt-ingress-https" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.tso-cp-mgt-sg.id}"
  cidr_blocks       = ["${var.vdss-mgt-ip}"]
}

# PILOT VPN ENI security group rules

resource "aws_security_group_rule" "sg-pilot-vpn-egress-udp-500" {
  type              = "egress"
  from_port         = 500
  to_port           = 500
  protocol          = "udp"
  security_group_id = "${aws_security_group.tso-cp-vpn-sg.id}"
  cidr_blocks       = ["${var.vdss-vpn-ip}"]
}

resource "aws_security_group_rule" "sg-pilot-vpn-egress-esp" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "50"
  security_group_id = "${aws_security_group.tso-cp-vpn-sg.id}"
  cidr_blocks       = ["${var.vdss-vpn-ip}"]
}

# PILOT Private ENI security group rules

resource "aws_security_group_rule" "sg-pilot-private-egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  security_group_id = "${aws_security_group.tso-cp-private-sg.id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "sg-pilot-private-ingress" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  security_group_id = "${aws_security_group.tso-cp-private-sg.id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

###
### End of MCBOSS-Provided code
###

# "All Servers" security group
resource "aws_security_group" "all-sg" {
  name        = "all-sg"
  description = "ports and protocols for all servers"
  vpc_id      = "${local.vpc_id}"

  tags {
    Name = "all-sg"
  }
}

# "All Linux Servers" security group
resource "aws_security_group" "linux-all-sg" {
  name        = "linux-all-sg"
  description = "ports and protocols for all Linux servers"
  vpc_id      = "${local.vpc_id}"

  tags {
    Name = "linux-all-sg"
  }
}

# Gitlab server security group
resource "aws_security_group" "gitlab-sg" {
  name        = "gitlab-sg"
  description = "ports and protocols for Gitlab servers"
  vpc_id      = "${local.vpc_id}"

  tags {
    Name = "gitlab-sg"
  }
}

#Jenkins server security group
resource "aws_security_group" "jenkins-sg" {
  name        = "jenkins-sg"
  description = "ports and protocols for Jenkins servers"
  vpc_id      = "${local.vpc_id}"

  tags {
    Name = "jenkins-sg"
  }
}

# PostgreSQL RDS security group
resource "aws_security_group" "postgres-rds-sg" {
  name        = "postgres-rds-sg"
  description = "ports and protocols for RDS postgres server"
  vpc_id      = "${local.vpc_id}"

  tags {
    Name = "postgres-rds-sg"
  }
}

#Temporary security group rules
resource "aws_security_group_rule" "sg-all-sg-egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  security_group_id = "${aws_security_group.all-sg.id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "sg-all-sg-ingress" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  security_group_id = "${aws_security_group.all-sg.id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "gitlab-sg-egress-tcp-5432-postgres-rds-sg" {
  type                     = "egress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.gitlab-sg.id}"
  source_security_group_id = "${aws_security_group.postgres-rds-sg.id}"
}

resource "aws_security_group_rule" "postgres-rds-sg-ingress-tcp-5432-gitlab-sg" {
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.postgres-rds-sg.id}"
  source_security_group_id = "${aws_security_group.gitlab-sg.id}"
}


/*
#TODO: Rules need to be refactored in terms of VPN/Bastion connectivity.

#TODO: Update SGs to use non-SSL ports due to SSL termination
# at the F5

resource "aws_security_group_rule" "jenkins-sg-egress-tcp-443-gitlab-sg" {
  type                     = "egress"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.jenkins-sg.id}"
  source_security_group_id = "${aws_security_group.gitlab-sg.id}"
}

resource "aws_security_group_rule" "jenkins-sg-ingress-tcp-22-gitlab-sg" {
  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.jenkins-sg.id}"
  source_security_group_id = "${aws_security_group.gitlab-sg.id}"
}

resource "aws_security_group_rule" "jenkins-sg-egress-tcp-22-gitlab-sg" {
  type                     = "egress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.jenkins-sg.id}"
  source_security_group_id = "${aws_security_group.gitlab-sg.id}"
}

#Gitlab server security group rules
resource "aws_security_group_rule" "gitlab-sg-ingress-tcp-443-jenkins-sg" {
  type                     = "ingress"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.gitlab-sg.id}"
  source_security_group_id = "${aws_security_group.jenkins-sg.id}"
}
resource "aws_security_group_rule" "gitlab-sg-ingress-tcp-443-all-sg" {
  type                     = "ingress"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.gitlab-sg.id}"
  source_security_group_id = "${aws_security_group.all-sg.id}"
}

resource "aws_security_group_rule" "all-sg-egress-tcp-443-gitlab-sg" {
  type                     = "egress"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.all-sg.id}"
  source_security_group_id = "${aws_security_group.gitlab-sg.id}"
}

resource "aws_security_group_rule" "gitlab-sg-ingress-tcp-22-jenkins-sg" {
  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.gitlab-sg.id}"
  source_security_group_id = "${aws_security_group.jenkins-sg.id}"
}

resource "aws_security_group_rule" "gitlab-sg-egress-tcp-22-jenkins-sg" {
  type                     = "egress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.gitlab-sg.id}"
  source_security_group_id = "${aws_security_group.jenkins-sg.id}"
}


resource "aws_security_group_rule" "jenkins-sg-egress-tcp-22-linux-all-sg" {
  type                     = "egress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.jenkins-sg.id}"
  source_security_group_id = "${aws_security_group.linux-all-sg.id}"
}

resource "aws_security_group_rule" "linux-all-sg-ingress-tcp-22-jenkins-sg" {
  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.linux-all-sg.id}"
  source_security_group_id = "${aws_security_group.jenkins-sg.id}"
}
*/
