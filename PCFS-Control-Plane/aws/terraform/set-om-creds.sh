export OM_TARGET=$(terraform output ops_manager_dns)
export OM_USERNAME=$(terraform output ops_manager_username)
export OM_PASSWORD=$(terraform output ops_manager_password)
