resource "aws_key_pair" "tso_cp" {
  key_name   = "${var.short_env_id}-tso-cp-key"
  public_key = "${tls_private_key.tso_cp_private_key.public_key_openssh}"
}

resource "tls_private_key" "tso_cp_private_key" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}
