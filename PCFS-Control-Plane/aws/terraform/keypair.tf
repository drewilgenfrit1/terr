resource "tls_private_key" "control-plane" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "control-plane" {
  key_name   = "${var.env_id}_control_plane"
  public_key = "${tls_private_key.control-plane.public_key_openssh}"
}
output "ops_manager_ssh_private_key" {
  value     = "${tls_private_key.control-plane.private_key_pem}"
  sensitive = true
}