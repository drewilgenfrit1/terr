# Perimeter route tables

resource "aws_route_table" "tso-cp-rt-z1-perimeter" {
  vpc_id = "${local.vpc_id}"

  tags {
    Name = "tso-cp-rt-z1-perimeter"
  }
}

# Private route tables

resource "aws_route_table" "tso-cp-rt-z1-private" {
  vpc_id = "${local.vpc_id}"

  tags {
    Name = "tso-cp-rt-z1-private"
  }
}

# Management route tables

resource "aws_route_table" "tso-cp-rt-z1-mgt" {
  vpc_id = "${local.vpc_id}"

  tags {
    Name = "tso-cp-rt-z1-mgt"
  }
}

# Perimeter route table subnet association

resource "aws_route_table_association" "tso-cp-rt-z1-perimeter" {
  subnet_id      = "${aws_subnet.tso-cp-subnet-z1-perimeter.id}"
  route_table_id = "${aws_route_table.tso-cp-rt-z1-perimeter.id}"
}

# Private route table subnet association

resource "aws_route_table_association" "tso-cp-rt-z1-private" {
  subnet_id      = "${aws_subnet.tso-cp-subnet-z1-private.id}"
  route_table_id = "${aws_route_table.tso-cp-rt-z1-private.id}"
}

# MGT route table subnet association

resource "aws_route_table_association" "tso-cp-rt-z1-mgt" {
  subnet_id      = "${aws_subnet.tso-cp-subnet-z1-mgt.id}"
  route_table_id = "${aws_route_table.tso-cp-rt-z1-mgt.id}"
}

# MGT route table rules

resource "aws_route" "tso-cp-rt-z1-mgt-vdss" {
 count                     = "${var.pilot_vdss_pcx == ""? 0 : 1}"
 route_table_id            = "${aws_route_table.tso-cp-rt-z1-mgt.id}"
 destination_cidr_block    = "${var.vdss-mgt-ip}"
 vpc_peering_connection_id = "${var.pilot_vdss_pcx}"
}

#Perimeter route table rules

resource "aws_route" "tso-cp-rt-perimeter" {
 count                     = "${var.pilot_vdss_pcx == ""? 0 : 1}"
 route_table_id            = "${aws_route_table.tso-cp-rt-z1-perimeter.id}"
 destination_cidr_block    = "${var.vdss-vpn-ip}"
 vpc_peering_connection_id = "${var.pilot_vdss_pcx}"
}

# Perimeter route table rules

resource "aws_route" "tso-cp-rt-private" {
  route_table_id         = "${aws_route_table.tso-cp-rt-z1-private.id}"
  destination_cidr_block = "0.0.0.0/0"
  network_interface_id   = "${aws_network_interface.tso-cp-eni-vpn-z1-01-private.id}"
}

###
### End of MCBOSS-Provided code
###
