#########################################################################
# VPC region


# Pilot VDSS VPC peering ID

variable "pilot_vdss_pcx" {
 description = "Name of the Pilot VDSS peering connection"
 default     = "pcx-cafeaea3"
}

# Availability zone variables

# variable "availability_zone_1" {
#   description = "Name of zone 1"
#   default     = "us-gov-west-1c"
# }

# variable "availability_zone_2" {
#   description = "Name of zone 2"
#   default     = "us-gov-west-1b"
# }

# variable "availability_zone_3" {
#   description = "Name of zone 3"
#   default     = "us-gov-west-1a"
# }

# variable "availability_zones" {
#   type = "list"
# }

# Default Juniper vSRX AMI for us-gov-west-1

variable "vsrx_default_ami" {
  default = "ami-a26e01c3"
}

# Default RHEL 7 base AMI for us-gov-west-1

variable "linux_default_ami" {
  default = "ami-0466e865"
}

# Default Server 2016 base AMI for us-gov-west-1

variable "windows_default_ami" {
  default = "ami-d761f6b6"
}

# Default Server 2012R2 base AMI for us-gov-west-1

variable "windows2012r2_default_ami" {
  default = "ami-1329ba72"
}

# VPC and Subnet variables

variable "tso-cp-cidr" {
  description = "IP CIDR block for the Pilot VDMS VPC"
  default     = "10.0.16.0/22"
}

variable "tso-cp-z1-perimeter-cidr" {
  description = "IP CIDR block for tso-cp-z1-perimeter"
  default     = "10.0.16.0/27"
}

variable "tso-cp-z1-private-cidr" {
  description = "IP CIDR block for tso-cp-z1-private"
  default     = "10.0.16.32/27"
}

variable "tso-cp-z1-mgt-cidr" {
  description = "IP CIDR block for tso-cp-z1-mgt"
  default     = "10.0.16.64/27"
}

# Instance name variables

variable "vpn-z1-01_name" {
  type    = "string"
  default = "tso-cp-vpn-z1-01"
}

# Instance IP address variables

variable "tso-cp-eni-vpn-z1-01-mgt-ip" {
  description = "IP for the vpn-z1-01 MGT ENI"
  type        = "string"
  default     = "10.0.16.68"
}

variable "tso-cp-eni-vpn-z1-01-private-ip" {
  description = "IP for the vpn-z1-01 Private ENI"
  type        = "string"
  default     = "10.0.16.36"
}

variable "tso-cp-eni-vpn-z1-01-perimeter-ip" {
  description = "IP for the vpn-z1-01 Perimeter ENI"
  type        = "string"
  default     = "10.0.16.4"
}

# MGT instance IP variable

variable "vdss-mgt-ip" {
  description = "IP for the VDSS MGT ENI"
  type        = "string"
  default     = "10.0.8.121/32"
}

# VDSS VPN instance MGT IP variable

variable "vdss-vpn-ip" {
  description = "IP for the VDSS VPN ENI"
  type        = "string"
  default     = "10.0.8.68/32"
}
