variable "opsman_ami_map" {
  type = "map"

  default = {
    ap-south-1     = "ami-0a9015e8d5ff3e094"
    eu-west-3      = "ami-001e89f66ecb1a0b4"
    eu-west-2      = "ami-0d7b59f3e81bdc9f1"
    eu-west-1      = "ami-037d3feca075d34f2"
    ap-northeast-2 = "ami-0eb5966e9f25bb86c"
    ap-northeast-1 = "ami-07502c2886a90e5f0"
    sa-east-1      = "ami-0358dd5af669ee292"
    ca-central-1   = "ami-0ad265deec9a7e72c"
    ap-southeast-1 = "ami-06c52383a5ca46c0d"
    ap-southeast-2 = "ami-0110c9f0186d9c502"
    eu-central-1   = "ami-0fdd698a3e631a480"
    us-east-1      = "ami-0536197382b025a6c"
    us-east-2      = "ami-06c9610169c1e5d69"
    us-west-1      = "ami-0e23bfe00650dab1e"
    us-west-2      = "ami-057fe3bdefd2dedb9"
    us-gov-west-1  = "ami-8298fde3"
  }
}

resource "aws_instance" "cp_ops_manager" {
  private_ip             = "${cidrhost(aws_subnet.opsman_subnet.cidr_block, 9)}"
  instance_type          = "m5.large"
  subnet_id              = "${aws_subnet.opsman_subnet.id}"
  ami                    = "${lookup(var.opsman_ami_map, var.region)}"
  vpc_security_group_ids = ["${aws_security_group.control_plane_ops_manager.id}"]
  key_name               = "${aws_key_pair.control-plane.key_name}"
  iam_instance_profile   = "${aws_iam_instance_profile.ops_manager.name}"
  depends_on             = ["aws_key_pair.jumpbox"]

  ebs_block_device {
    device_name           = "/dev/xvda"
    volume_size           = "200"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  lifecycle {
    ignore_changes  = ["ebs_block_device"]
    prevent_destroy = false
  }

  tags {
    Name  = "${var.env_id}-control-plane-opsman"
    EnvID = "${var.env_id}"
  }
}

resource "random_id" "ops_manager_password_generator" {
  byte_length = 16
}

resource "random_id" "ops_manager_decryption_phrase_generator" {
  byte_length = 16
}

resource "random_id" "concourse_password_generator" {
  byte_length = 16
}

output "concourse_password" {
  value     = "${var.concourse_password == "" ? random_id.concourse_password_generator.b64 : var.concourse_password}"
  sensitive = true
}

output "ops_manager_username" {
  value = "${var.ops_manager_username}"
}

output "ops_manager_dns" {
  value = "${var.ops_manager_url}"
}

output "ops_manager_password" {
  value     = "${var.ops_manager_password == "" ? random_id.ops_manager_password_generator.b64 : var.ops_manager_password}"
  sensitive = true
}

output "ops_manager_decryption_phrase" {
  value     = "${var.ops_manager_decryption_phrase == "" ? random_id.ops_manager_decryption_phrase_generator.b64 : var.ops_manager_decryption_phrase}"
  sensitive = true
}

variable "concourse_password" {
  description = "Password for administrator user. Generated if left blank."
  type        = "string"
  default     = ""
}

variable "ops_manager_url" {
  type    = "string"
  default = ""
}

variable "ops_manager_username" {
  type    = "string"
  default = "admin"
}

variable "ops_manager_password" {
  description = "Password for administrator user. Generated if left blank."
  type        = "string"
  default     = ""
}

variable "ops_manager_decryption_phrase" {
  description = "Decryption Phrase for Ops Manager Authentication. Generated if left blank."
  type        = "string"
  default     = ""
}
