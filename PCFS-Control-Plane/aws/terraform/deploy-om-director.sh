#!/bin/bash
set -e

#export SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

export director_deployment_azs_yaml=$(terraform output director_azs_yaml)
export director_aws_access_key_id=$(terraform output aws_access_key_id)
export director_aws_secret_access_key=$(terraform output aws_secret_access_key)
export director_key_pair_name=$(terraform output default_key_name)
export director_region=$(terraform output region)
export director_security_group=$(terraform output internal_security_group)
export director_ssh_private_key=$(terraform output bosh_vms_key)
export director_subnets=$(terraform output cloud_config_subnets_yaml)
export director_network_name=$(terraform output network_name)
export director_singleton_az=$(terraform output singleton_availability_zone)
export director_instance_profile=$(terraform output ops_manager_iam_instance_profile_name)

source ../../set-om-creds.sh

om -t $(terraform output ops_manager_dns) --skip-ssl-validation \
  configure-authentication \
    --decryption-passphrase $(terraform output ops_manager_decryption_phrase) \
    --username $(terraform output ops_manager_username) \
    --password $(terraform output ops_manager_password)

echo "Configuring Ops Manager Director"

om -t $OM_TARGET --skip-ssl-validation \
  configure-director --config director.yml --vars-env=director

echo "Deploying Ops Manager Director"
om -t $OM_TARGET --skip-ssl-validation apply-changes
