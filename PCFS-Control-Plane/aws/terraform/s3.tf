resource "aws_s3_bucket" "opsman_bucket" {
  acl    = "private"
  
  tags {
    Name        = "opsman_bucket"
    Environment = "Control-Plane"
  }
}

output "opsman_bucket" {
  value = "${aws_s3_bucket.opsman_bucket.bucket}"
}