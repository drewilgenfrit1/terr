variable "jumpbox_ami_map" {
  type = "map"

  default = {
    ap-northeast-1 = "ami-10dfc877"
    ap-northeast-2 = "ami-1a1bc474"
    ap-south-1     = "ami-74c1861b"
    ap-southeast-1 = "ami-36af2055"
    ap-southeast-2 = "ami-1e91817d"
    ca-central-1   = "ami-12d36a76"
    eu-central-1   = "ami-9ebe18f1"
    eu-west-1      = "ami-3a849f5c"
    eu-west-2      = "ami-21120445"
    us-east-1      = "ami-d4c5efc2"
    us-east-2      = "ami-f27b5a97"
    us-gov-west-1  = "ami-c39610a2"
    us-west-1      = "ami-b87f53d8"
    us-west-2      = "ami-8bfce8f2"
  }
}

resource "aws_instance" "cp_jumpbox" {
  private_ip             = "${cidrhost(aws_subnet.opsman_subnet.cidr_block, 8)}"
  instance_type          = "t2.medium"
  subnet_id              = "${aws_subnet.opsman_subnet.id}"
  source_dest_check      = false
  ami                    = "${lookup(var.jumpbox_ami_map, var.region)}"
  vpc_security_group_ids = ["${aws_security_group.jumpbox.id}"]
  key_name               = "${aws_key_pair.bosh_vms.key_name}"

  tags {
    Name  = "${var.env_id}-jumpbox"
    EnvID = "${var.env_id}"
  }
}

resource "aws_key_pair" "jumpbox" {
  key_name   = "${var.env_id}-jumpbox"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQClUE7Kw9hljbYIwyPPgnKM0t2exVviYrpAiG2enYMIaldMTk2DPD++YiUupLkkh6L9H03oRGy6GYB7pg142q6M2jq7UtcjRMhTrZ+RUUdiT5Cz1BlVYEu1fLDB0AaxaQr3sXG1iVo9PWzKODBoj02BsY5GeE86wn1OTEvTdWfSqEM15DXEaXqL6kv+Hp0W6sBz0wuMMNxObVLOUOQDFa7ECaJnYzX6LMgtHiMTYAlh/6SZn1BScicaNVIO3Tqod/ElNfblb0IBFUcbeQPAg8J5Mt8d2MDK8qM8UFTNvhAVVGAKSOlottAxQs5ZvxA0M3uUJnesEJlpBrxVB++HTF0n abryant@alexs-mbp.lan"
}
