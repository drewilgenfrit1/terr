#!/bin/bash

set -e

export SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

source ./set-bosh-proxy-creds.sh

bosh update-config --type=cloud --name=concourse \
  -v lb_target_groups="[$(terraform output concourse_lb_target_groups)]" \
  -v vm_security_group="[$(terraform output concourse_lb_internal_security_group), $(terraform output internal_security_group) ]" \
   ./aws/concourse-cloud-config.yml
