# Ops Manager Control Plane for vSphere

This directory contains all the vSphere specific bits to deploy Concourse
and other control plane components.

## Deployment

**Note:** All commands listed here should be run from the root directory of this repository.

Neither the `vsphere_virtual_machine` resource nor the vSphere Terraform provider supports importing of OVA or OVF files as this is a workflow that is fundamentally not the domain of Terraform. The supported path for vSphere deployment in Terraform is to first import the virtual machine into a template that has not been powered on, and then clone from that template.

### Import Ops Manager OVA

Install [govc](https://github.com/vmware/govmomi/releases) and set the `GOVC_*` [environment variables](https://github.com/vmware/govmomi/blob/master/govc/USAGE.md) inside `.envrc` in the root directory, accordingly.
Run `direnv allow` or `source .envrc` to export them.
```
$ source .envrc
$ ./vsphere/upload-ova.sh pivnetrefreshtoken-r '2.3.5' /tmp
```

### vSphere Service Account for Terraform

Terraform requires a user or service account with the minimum set of privileges to bootstrap the control plane OpsMan VM and other resources. This does NOT have to be the same [service account](https://docs.pivotal.io/pivotalcf/2-3/customizing/vsphere-service-account.html) OpsMan requires to deploy PCF.

The vCenter Role must have the following privileges:

  1. Datastore
  1. Folder (all)
  1. Network
  1. Resource
  1. Virtual Machine
  1. vCenter Inventory Service
  1. License

Refer to [vSphere Terraform Provider documentation](https://www.terraform.io/docs/providers/vsphere/index.html).

### Configure Terraform Variables

Start by making a copy of the terraform vars template for your IaaS and filling it out.

For example, on vSphere:

```
$ cp vsphere/terraform/example.tfvars control-plane.tfvars
```

### Var File

Copy the stub content below into a file called `terraform.tfvars` and put it in the root of this project.
These vars will be used when you run `terraform  apply`.
You should fill in the stub values with the correct content.

```hcl
vcenter_user          = "some-user"
vcenter_password      = "some-password"
vcenter_server        = "some-server"
vcenter_dc            = "some-datacenter"
vcenter_cluster       = "some-cluster"
vcenter_rp            = "some-cluster/some-resource-pool"
vcenter_ds            = "some-datastore"
om_ipv4_address       = "x.x.x.x"
om_netmask0           = "255.x.x.x"
om_gateway            = "x.x.x.x"
om_DNS                = "x.x.x.x"
om_ntp_servers        = "x.x.x.x"
om_admin_password     = "some-password"
om_custom_hostname    = "some-hostname"
vcenter_network       = "some-network"
vcenter_vms           = "some-parent-folder/some-vms-folder"
om_template           = "some-templates-folder"
```

### Var Details

- vsphere_user: **(required)** Username for vSphere API operations.
- vsphere_password: **(required)** Password for vSphere API operations.
- vsphere_server: **(required)** vCenter server name for vSphere API operations.
- vcenter_dc: **(required)** Datacenter for launching vms.
- vcenter_cluster: **(required)** Cluster for launching vms.
- vcenter_ds: **(required)** Datastore for the virtual disks.
- om_ipv4_address: **(optional)** OpsMgr static IPv4 address. Leave unset if DHCP is desired.
- om_netmask0: **(optional)** OpsMgr netmask. Leave unset if DHCP is desired.
- om_gateway: **(optional)** OpsMgr gateway. Leave unset if DHCP is desired.
- om_DNS: **(optional)** OpsMgr DNS Servers, comma separated. Leave unset if DHCP is desired.
- om_ntp_servers: **(required)** OpsMgr NTP Servers, comma separated.
- om_admin_password: **(required)** OpsMgr VM password. The username is 'ubuntu'.
- om_custom_hostname: **(optional)** OpsMgr Hostname. Default is 'pivotal-ops-manager'.
- vcenter_network: **(required)** Label for the network interface.
- vcenter_vms: **(required)** Folder for launching vms.

- vcenter_rp: **(optional)** Resource Pool for launching vms.

- om_template: **(optional)** Ops Manager vm template.
- om_vmdk: **(optional)** If not specifying `vcenter_template`, specify path to a vmdk in the `vcenter_ds`.

- allow_unverified_ssl: **(optional)** Defaults to false. Set to true if your vcenter uses self-signed certs.

**Note:** All commands listed here should be run from the root directory of this repository.

### Pave Your IaaS

This creates a single network and deploys an Ops Manager VM to it.
It also creates a load balancer for use with Concourse.

```
$ terraform init ./vsphere/terraform
$ terraform plan -var-file=control-plane.tfvars ./vsphere/terraform
$ terraform apply -auto-approve -var-file=control-plane.tfvars ./vsphere/terraform
```

### Configure OpsMan and Deploy Director

Configure Ops Manager and deploy the director using the script in the directory for your IaaS:

```
$ ./vsphere/deploy-om-director.sh
```

### Deploy Concourse

Add a named cloud-config for the Concourse deployment:

First, copy the name of the cpi in the default cloud config:
```
$ source ./set-bosh-proxy-creds.sh
$ bosh config --type=cpi --name=default
```

Then update the concourse cloud config:
```
$ ./vsphere/update-concourse-cloud-config.sh <bosh-cpi-name>
```

Upload the Xenial stemcell required by Concourse:
Check [bosh.io](https://bosh.io/stemcells/bosh-vsphere-esxi-ubuntu-xenial-go_agent)
for the latest **vSphere Xenial Full stemcell**.

Make sure you are in a **bash shell** before running the source command, it will not work in zsh.
```
$ source ./set-bosh-proxy-creds-vsphere.sh
$ bosh upload-stemcell --sha1 48c2d97cf3a2d98bfd98c3df7088f2bdb1db0360 \
    https://bosh.io/d/stemcells/bosh-vsphere-esxi-ubuntu-xenial-go_agent?v=170.6
```

Finally deploy concourse:

You will need to clone [concourse-bosh-deployment](https://github.com/concourse/concourse-bosh-deployment) first.
Then before running the deploy script below, set **CONCOURSE_BOSH_DEPLOYMENT** to the path of your checkout.

```
$ git clone https://github.com/concourse/concourse-bosh-deployment ~/workspace/concourse-bosh-deployment
$ export CONCOURSE_BOSH_DEPLOYMENT=~/workspace/concourse-bosh-deployment
$ cp vsphere/deploy-concourse.sh deploy-vsphere-concourse.sh
$ ./deploy-vsphere-concourse.sh
```

### Connecting to Concourse Credhub

The client secret for connecting to the Concourse Credhub is stored in the BOSH
director's Credhub.  This repo includes a `target-concourse-credhub.sh` script
that you can source in order to target the Concourse Credhub.

```
$ source target-concourse-credhub.sh
$ credhub find
```

### Connecting to Concourse

Log in with the `fly` CLI:

```
$ ./login-concourse.sh
```

## Tear Down

### Delete all Resources not managed by Terraform

There are a number of options to do this. If Concourse has been deployed,
running `bosh delete-deployment -d concourse` will clean these VMs up.
You will also need to delete the stemcells and disks with `bosh clean-up --all`.

### Cleanup Terraform Resources

Use terraform destroy to cleanup resources created by Terraform including VMs,
images, networks, roles, firewalls, etc:

```
terraform destroy -var-file=control-plane.tfvars ./vsphere/terraform
```

If terraform destroy fails you may still have VMs running that are not managed
by terraform and will need to delete them by hand.
