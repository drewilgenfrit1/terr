output "concourse_network" {
  value = "${nsxt_logical_switch.control-plane-ls.display_name}"
}

output "opsmanager_internal_ip" {
  value = "${var.cp_om_internal_ipv4_address}"
}

output "opsmanager_external_ip" {
  value = "${var.cp_om_ipv4_address}"
}

output "bosh_internal_ip" {
  value = "${var.cp_bosh_internal_ipv4_address}"
}

output "bosh_external_ip" {
  value = "${var.cp_bosh_ipv4_address}"
}

output "concourse_web_internal_ip" {
  value = "${var.nsxt_atc_member1}"
}

output "concourse_web_external_ip" {
  value = "${var.concourse_vip_server}"
}
