resource "nsxt_lb_icmp_monitor" "lb_icmp_monitor" {
  display_name = "${var.env_name}_lb_icmp_monitor"
  fall_count   = 3
  interval     = 5
}

resource "nsxt_lb_pool" "atc_lb_pool" {
  description              = "${var.env_name}_atc_lb_pool provisioned by Terraform"
  display_name             = "${var.env_name}_atc_lb_pool"
  algorithm                = "WEIGHTED_ROUND_ROBIN"
  min_active_members       = 1
  tcp_multiplexing_enabled = false
  tcp_multiplexing_number  = 3
  active_monitor_id        = "${nsxt_lb_icmp_monitor.lb_icmp_monitor.id}"

  member {
    admin_state                = "ENABLED"
    backup_member              = "false"
    display_name               = "1st-member"
    ip_address                 = "${var.nsxt_atc_member1}"
    max_concurrent_connections = "100"
    weight                     = "1"
  }
}

resource "nsxt_lb_http_application_profile" "http_xff" {
  x_forwarded_for = "INSERT"
}

resource "nsxt_lb_cookie_persistence_profile" "session_persistence" {
  cookie_name = "SESSION"
}

resource "nsxt_lb_client_ssl_profile" "ssl1" {
  prefer_server_ciphers = true
}

resource "nsxt_lb_server_ssl_profile" "ssl1" {
  session_cache_enabled = false
}

resource "nsxt_lb_http_virtual_server" "concourse_lb_virtual_server" {
  description                = "concourse lb_virtual_server provisioned by terraform"
  display_name               = "${var.env_name}_concourse virtual server"
  access_log_enabled         = true
  application_profile_id     = "${nsxt_lb_http_application_profile.http_xff.id}"
  enabled                    = true
  ip_address                 = "${var.concourse_vip_server}"
  port                       = "443"
  default_pool_member_port   = "443"
  max_concurrent_connections = 50
  max_new_connection_rate    = 20
  persistence_profile_id     = "${nsxt_lb_cookie_persistence_profile.session_persistence.id}"
  pool_id                    = "${nsxt_lb_pool.atc_lb_pool.id}"
  # sorry_pool_id              = "${nsxt_lb_pool.sorry_pool.id}"
  # rule_ids                   = ["${nsxt_lb_http_request_rewrite_rule.redirect_post.id}"]

  client_ssl {
    client_ssl_profile_id   = "${nsxt_lb_client_ssl_profile.ssl1.id}"
    default_certificate_id  = "${data.nsxt_certificate.concourse_certificate.id}"
    certificate_chain_depth = 2
    client_auth             = false
    # ca_ids                  = ["${data.nsxt_certificate.ca.id}"]
    # crl_ids                 = ["${data.nsxt_certificate.crl.id}"]
    # sni_certificate_ids     = ["${data.nsxt_certificate.sni.id}"]
  }

  server_ssl {
    server_ssl_profile_id   = "${nsxt_lb_server_ssl_profile.ssl1.id}"
    # client_certificate_id   = "${data.nsxt_certificate.client.id}"
    # certificate_chain_depth = 2
    server_auth             = false
    # ca_ids                  = ["${data.nsxt_certificate.server_ca.id}"]
    # crl_ids                 = ["${data.nsxt_certificate.crl.id}"]
  }

}

resource "nsxt_lb_http_virtual_server" "concourse_uaa_lb_virtual_server" {
  description                = "concourse uaa lb_virtual_server provisioned by terraform"
  display_name               = "${var.env_name}_concourse uaa virtual server"
  access_log_enabled         = true
  application_profile_id     = "${nsxt_lb_http_application_profile.http_xff.id}"
  enabled                    = true
  ip_address                 = "${var.concourse_vip_server}"
  port                       = "8443"
  default_pool_member_port   = "8443"
  max_concurrent_connections = 50
  max_new_connection_rate    = 20
  persistence_profile_id     = "${nsxt_lb_cookie_persistence_profile.session_persistence.id}"
  pool_id                    = "${nsxt_lb_pool.atc_lb_pool.id}"
  # sorry_pool_id              = "${nsxt_lb_pool.sorry_pool.id}"
  # rule_ids                   = ["${nsxt_lb_http_request_rewrite_rule.redirect_post.id}"]

  client_ssl {
    client_ssl_profile_id   = "${nsxt_lb_client_ssl_profile.ssl1.id}"
    default_certificate_id  = "${data.nsxt_certificate.concourse_certificate.id}"
    certificate_chain_depth = 2
    client_auth             = false
    # ca_ids                  = ["${data.nsxt_certificate.ca.id}"]
    # crl_ids                 = ["${data.nsxt_certificate.crl.id}"]
    # sni_certificate_ids     = ["${data.nsxt_certificate.sni.id}"]
  }

  server_ssl {
    server_ssl_profile_id   = "${nsxt_lb_server_ssl_profile.ssl1.id}"
    # client_certificate_id   = "${data.nsxt_certificate.client.id}"
    # certificate_chain_depth = 2
    server_auth             = false
    # ca_ids                  = ["${data.nsxt_certificate.server_ca.id}"]
    # crl_ids                 = ["${data.nsxt_certificate.crl.id}"]
  }

}

resource "nsxt_lb_service" "lb_service" {
  description  = "concourse lb_service"
  display_name = "${var.env_name}_lb_service"

  enabled           = true
  logical_router_id = "${nsxt_logical_tier1_router.control-plane-t1.id}"
  virtual_server_ids = ["${nsxt_lb_http_virtual_server.concourse_uaa_lb_virtual_server.id}", "${nsxt_lb_http_virtual_server.concourse_lb_virtual_server.id}"]
  error_log_level   = "INFO"
  size              = "SMALL"

  depends_on        = ["nsxt_logical_router_link_port_on_tier1.control-plane-t1-to-t0"]
}
