output "iaas" {
  value = "vsphere"
}

output "om" {
  value = "${vsphere_virtual_machine.vm.name}"
}

output "om_ipv4_address" {
  value = "${var.om_ipv4_address}"
}

output "ops_manager_dns" {
  value = "${var.om_custom_hostname}"
}

output "ops_manager_username" {
  value = "${var.ops_manager_username}"
}

output "ops_manager_password" {
  value     = "${var.ops_manager_password == "" ? random_id.ops_manager_password_generator.b64 : var.ops_manager_password}"
  sensitive = true
}

output "ops_manager_decryption_phrase" {
  value     = "${var.ops_manager_decryption_phrase == "" ? random_id.ops_manager_decryption_phrase_generator.b64 : var.ops_manager_decryption_phrase}"
  sensitive = true
}

output "concourse_password" {
  value     = "${var.concourse_password == "" ? random_id.concourse_password_generator.b64 : var.concourse_password}"
  sensitive = true
}

output "concourse_ip" {
  value = "${var.concourse_ip}"
}

output "concourse_dns" {
  value = "${var.concourse_dns}"
}

output "concourse_zone" {
  value = "${var.concourse_zone}"
}

output "vcenter_server" {
  value = "${var.vcenter_server}"
}

output "vcenter_user" {
  value = "${var.vcenter_user}"
}

output "vcenter_password" {
  value     = "${var.vcenter_password}"
  sensitive = true
}

output "vcenter_dc" {
  value = "${var.vcenter_dc}"
}

output "vcenter_cluster" {
  value = "${var.vcenter_cluster}"
}

output "vcenter_network" {
  value = "${var.vcenter_network}"
}

output "vcenter_rp" {
  value = "${var.vcenter_rp}"
}

output "availability_zone_name" {
  value = "${var.zone}"
}

output "vcenter_ephemeral_datastore" {
  value = "${var.vcenter_ds}"
}

output "vcenter_persistent_datastore" {
  value = "${var.vcenter_ds}"
}

output "vcenter_disk_folder" {
  value = "${var.om_disks}"
}

output "vcenter_template_folder" {
  value = "${var.vcenter_templates}"
}

output "vcenter_vm_folder" {
  value = "${var.vcenter_vms}"
}

output "ntp_servers" {
  value = "${var.om_ntp_servers}"
}

output "dns_servers" {
  value = "${var.om_DNS}"
}

output "subnet_cidr" {
  value = "${var.subnet_cidr}"
}

output "subnet_gateway" {
  value = "${var.om_gateway}"
}

output "subnet_identifier" {
  value = "${var.vcenter_network}"
}

output "subnet_reserved_ips" {
  value = "${var.subnet_reserved_ips != "" ? var.subnet_reserved_ips : format("%s-%s", cidrhost(var.subnet_cidr, 0), cidrhost(var.subnet_cidr, 10)) }"
}

output "bosh_post_deploy_enabled" {
  value = "${var.bosh_post_deploy_enabled == "" ? "false" : var.bosh_post_deploy_enabled}"
}

output "bosh_vm_folder" {
  value = "${var.bosh_vm_folder}"
}
output "bosh_template_folder" {
  value = "${var.bosh_template_folder}"
}
output "bosh_disk_path" {
  value = "${var.bosh_disk_path}"
}
