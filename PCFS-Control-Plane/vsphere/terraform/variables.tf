variable "vcenter_user" {
  type = "string"
}

variable "vcenter_password" {
  type = "string"
}

variable "vcenter_server" {
  type = "string"
}

variable "vcenter_dc" {
  type = "string"
}

variable "vcenter_cluster" {
  type = "string"
}

variable "vcenter_network" {
  type    = "string"
  default = ""
}

variable "vcenter_rp" {
  type    = "string"
  default = ""
}

variable "vcenter_ds" {
  type = "string"
}

variable "om_ipv4_address" {
  type        = "string"
  default     = ""
}

variable "om_DNS" {
  type    = "string"
  default = ""
}

variable "om_admin_password" {
  type    = "string"
  default = "admin"
}

variable "subnet_cidr" {
  type    = "string"
  default = ""
}

variable "om_ntp_servers" {
  type    = "string"
  default = "time.nist.gov"
}

variable "om_custom_hostname" {
  type    = "string"
  default = ""
}

variable "om_gateway" {
  type    = "string"
  default = ""
}

variable "subnet_reserved_ips" {
  description = "BOSH will not assign IP addresses in this range. If not specified, the first 10 IPs of subnet_cidr will be marked as reserved, e.g. 10.0.0.0-10.0.0.10"
  type        = "string"
  default     = ""
}

variable "zone" {
  description = "Name for the availability zone you want to place the control plane VMs in. This is not the name of the cluster or resource pool."
  type    = "string"
  default = "z1"
}

variable "vcenter_vms" {
  type    = "string"
  default = ""
}

variable "vcenter_templates" {
  type    = "string"
  default = ""
}

variable "om_template" {
  type    = "string"
  default = ""
}

variable "om_disks" {
  type    = "string"
  default = ""
}

variable "om_vmdk" {
  type    = "string"
  default = ""
}

variable "allow_unverified_ssl" {
  default = false
  type    = "string"
}

variable "ops_manager_username" {
  type    = "string"
  default = "admin"
}

variable "ops_manager_password" {
  description = "Password for administrator user. Generated if left blank."
  type        = "string"
  default     = ""
}

variable "ops_manager_decryption_phrase" {
  description = "Decryption Phrase for Ops Manager Authentication. Generated if left blank."
  type        = "string"
  default     = ""
}

variable "concourse_password" {
  description = "Password for Concourse admin user. Generated if left blank."
  type        = "string"
  default     = ""
}

variable "concourse_ip" {
  description = "IP address to give to the concourse web instance_group"
  type        = "string"
  default     = ""
}

variable "concourse_zone" {
  description = "A separate availability zone for concourse"
  type        = "string"
  default     = ""
}

variable "concourse_dns" {
  description = "DNS A record name for the static web IP address"
  type        = "string"
  default     = ""
}

variable "bosh_post_deploy_enabled" {
  type   = "string"
  default = "false"
}

variable "bosh_vm_folder" {
  type   = "string"
  default = "bosh_vms"
}

variable "bosh_template_folder" {
  type   = "string"
  default = "bosh_templates"
}

variable "bosh_disk_path" {
  type   = "string"
  default = "bosh_disk"
}
