#!/bin/bash

set -e

export SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

CPI="$1"

if [[ -z "$CPI" ]]; then
  echo "Enter the name of the cpi in the default cloud config using the following command: "
  echo "bosh config --type=cpi --name=default"
  read -r CPI
fi

source ./set-bosh-proxy-creds-vsphere.sh

bosh -n update-config --type=cloud --name=concourse \
  -v vcenter_dc="$(terraform output vcenter_dc)" \
  -v vcenter_cluster="$(terraform output vcenter_cluster)" \
  -v vcenter_rp="$(terraform output vcenter_rp)" \
  -v network_name="$(terraform output vcenter_network)" \
  -v dns_servers="$(terraform output dns_servers)" \
  -v internal_gw="$(terraform output subnet_gateway)" \
  -v internal_cidr="$(terraform output subnet_cidr)" \
  -v reserved_ip_ranges="$(terraform output subnet_reserved_ips)" \
  -v concourse_ip="$(terraform output concourse_ip)" \
  -o $SCRIPT_DIR/operations/add-static-web-ip.yml \
  -v availability_zone="$(terraform output concourse_zone)" \
  -v cpi="$CPI" \
  $SCRIPT_DIR/concourse-cloud-config.yml
