#!/bin/bash

source ./set-bosh-proxy-creds-vsphere.sh

# local path to https://github.com/concourse/concourse-bosh-deployment
if [ -z ${CONCOURSE_BOSH_DEPLOYMENT} ]; then
  CONCOURSE_BOSH_DEPLOYMENT=~/concourse-bosh-deployment
fi

export WEB_VM_TYPE=medium
export DB_VM_TYPE=medium
export DB_PERSISTENT_DISK_TYPE=100GB

export CONCOURSE_HOST=$(terraform output concourse_dns)
export CONCOURSE_PASSWORD=$(terraform output concourse_password)
export CONCOURSE_IP=$(terraform output concourse_ip)


bosh -n deploy -d concourse $CONCOURSE_BOSH_DEPLOYMENT/cluster/concourse.yml \
   -l $CONCOURSE_BOSH_DEPLOYMENT/versions.yml \
   -l versions.yml \
   -v deployment_name=concourse \
   -v network_name=concourse \
   -v web_network_name=concourse \
   -v web_ip=${CONCOURSE_IP} \
   -v external_host=${CONCOURSE_HOST} \
   -v external_url="https://${CONCOURSE_HOST}" \
   -v web_vm_type=$WEB_VM_TYPE \
   -v db_vm_type=$DB_VM_TYPE \
   -v worker_vm_type="concourse.worker" \
   -v db_persistent_disk_type=$DB_PERSISTENT_DISK_TYPE \
   -v local_user.username=admin \
   -v local_user.password=$CONCOURSE_PASSWORD \
   -o $CONCOURSE_BOSH_DEPLOYMENT/cluster/operations/basic-auth.yml \
   -o $CONCOURSE_BOSH_DEPLOYMENT/cluster/operations/privileged-http.yml \
   -o $CONCOURSE_BOSH_DEPLOYMENT/cluster/operations/privileged-https.yml \
   -o $CONCOURSE_BOSH_DEPLOYMENT/cluster/operations/static-web.yml \
   -o $CONCOURSE_BOSH_DEPLOYMENT/cluster/operations/tls.yml \
   -o $CONCOURSE_BOSH_DEPLOYMENT/cluster/operations/tls-vars.yml \
   -o concourse/update-azs.yml \
   -v availability_zone="$(terraform output concourse_zone)" \
   -o concourse/add-credhub-uaa-to-web.yml
   # -o operations/enable-db-backups.yml \
   # -o operations/backup-atc-db.yml \
   # -o operations/backup-uaa-db.yml \
   # -o operations/backup-credhub-db.yml
