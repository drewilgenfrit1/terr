#!/bin/bash

set -e

export SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

export director_vcenter_host=$(terraform output vcenter_server)
export director_vcenter_user=$(terraform output vcenter_user)
export director_vcenter_password=$(terraform output vcenter_password)
export director_vsphere_datacenter=$(terraform output vcenter_dc)
export director_ephemeral_datastores=$(terraform output vcenter_ephemeral_datastore)
export director_persistent_datastores=$(terraform output vcenter_persistent_datastore)
export director_bosh_vm_folder=$(terraform output bosh_vm_folder)
export director_bosh_template_folder=$(terraform output bosh_template_folder)
export director_bosh_disk_path=$(terraform output bosh_disk_path)
export director_ntp=$(terraform output ntp_servers)
export director_bosh_post_deploy_enabled=$(terraform output bosh_post_deploy_enabled)
export director_az1_cluster=$(terraform output vcenter_cluster)
export director_az1_resource_pool=$(terraform output vcenter_rp)
export director_availability_zone=$(terraform output availability_zone_name)
export director_network_name=$(terraform output vcenter_network)
export director_internal_cidr=$(terraform output subnet_cidr)
export director_reserved_ip_ranges=$(terraform output subnet_reserved_ips)
export director_dns_servers=$(terraform output dns_servers)
export director_internal_gw=$(terraform output subnet_gateway)

source ./set-om-creds.sh

echo "Configuring Ops Manager Authentication"
om -t $OM_TARGET --skip-ssl-validation \
  configure-authentication \
    --decryption-passphrase $(terraform output ops_manager_decryption_phrase) \
    --username $(terraform output ops_manager_username) \
    --password $(terraform output ops_manager_password)

echo "Configuring Ops Manager Director"
om -t $OM_TARGET --skip-ssl-validation \
  configure-director --config $SCRIPT_DIR/director.yml --vars-env=director

echo "Deploying Ops Manager Director"
om -t $OM_TARGET --skip-ssl-validation apply-changes
