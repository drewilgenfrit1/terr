## NSX-T Terraform

* Create T1 Router and Logical switch as a subnet for control plane
* Create DNAT rule for Ops Manager and bosh
* Create Load Balancer for credhub, uaa and atc
* Configure SSL Certificate for Load Balancer

### Prerequisites

* A NSX-T env with NSX-T Manager host, username and password
* A configured T0 router
* Import a Certificate named as **concourse-certificate** to NSX-T manager

### Plugin Binary

**Warning!!!** **NSX-T Terraform 1.0 Release has limited feature... To support Load Balancer, SSL you have to build binary plugin from the [latest commit](https://github.com/terraform-providers/terraform-provider-nsxt/tree/08873350f11e10c2878dc5b48d96868f4801e816) of github**

### NSX-T Parameters

create a terraform.tfvars by using [example.tfvars](terraform/nsx-t/example.tfvars)

| Parameter |  Description |
|---|---|
| env_name  |  Unique identifier of the deployment |
|  cp_om_ipv4_address |  routable ip for ops manager |
|  cp_om_internal_ipv4_address |  ops manager internal ip |
|  cp_bosh_ipv4_address | bosh routable ip address |
|  cp_bosh_internal_ipv4_address | bosh internal ip address |
|  control_plane_cidr | control plane subnet cidr block |
|  control_plane_snat_ip | control plane subnet snat routable ip address |
|  control_plane_gateway | control plane subnet gateway|
|  nsxt_host | nsx manager host|
|  nsxt_username | nsx manager admin user|
|  nsxt_password | nsx manager admin password|
|  nsxt_transport_zone | nsx transport zone name|
| nsxt_edge_cluster | nsx edge cluster name |
| nsxt_t0_router | nsx t0 router name |
| nsxt_atc_member1 | concourse atc private ip |
| concourse_vip_server | concourse routable VIP IP |

### Run the terraform

```
pushd vsphere/terraform/nsx-t
terraform apply
popd
```
### Example Output

```
terraform output
bosh_external_ip = 10.193.148.248
bosh_internal_ip = 10.13.0.11
concourse_network = shaozhen-sandbox-control-plane-ls
concourse_web_external_ip = 10.193.148.247
concourse_web_internal_ip = 10.13.0.12
opsmanager_external_ip = 10.193.148.249
opsmanager_internal_ip = 10.13.0.6
```
