#!/bin/bash

set -euo pipefail

api_token="${1:?}"
om_version="${2:?}"
ova_path="${3:?}"

usage() {
  echo "$0 pivnetrefreshtoken-r '2.3.5' /path/to/ova"
}

if [ -z "$api_token" ]; then
  usage
  exit 1
fi

if ! test -n "$(find "$ova_path" -maxdepth 1 -name '*.ova' -print -quit)"; then
  om download-product --pivnet-api-token="$api_token" --pivnet-file-glob="*.ova" --pivnet-product-slug="ops-manager" --product-version="$om_version" --output-directory="$ova_path"
fi

opsman_ova="$(find "$ova_path" -maxdepth 1 -name '*.ova' -print -quit)"

# required
: "${GOVC_URL:?}"
: "${GOVC_USERNAME:?}"
: "${GOVC_PASSWORD:?}"
: "${GOVC_FOLDER:?}"
: "${GOVC_DATACENTER:?}"
: "${GOVC_DATASTORE:?}"
: "${GOVC_NETWORK:?}"
# optional
: "${TEMPLATE_NAME:=ops-mgr-tmpl}"
: "${GOVC_INSECURE:=true}"
: "${GOVC_RESOURCE_POOL:=}"

GOVC_HOST="" govc folder.create "/${GOVC_DATACENTER}/vm/${GOVC_FOLDER}" \
    || echo "Folder /${GOVC_DATACENTER}/vm/${GOVC_FOLDER} already exists"

GOVC_HOST="" govc import.ova \
    -name="${TEMPLATE_NAME}" \
    -options=<(echo "{\"NetworkMapping\": [{\"Name\": \"Network 1\", \"Network\": \"${GOVC_NETWORK}\"}]}") \
    "${opsman_ova}"
