resource "aws_s3_bucket" "services_backup_bucket" {
  bucket        = "${var.env_name}-services-backup-${local.bucket_suffix}"
  force_destroy = true

  versioning {
    enabled = true
  }

  tags = "${merge(var.tags, map("Name", "PCF Services Backup S3 Bucket"))}"
}


data "template_file" "services_backup_bucket_policy" {
  template = "${file("services_backup_buckets_policy.json")}"

  vars {
    services_backup_bucket_arn = "${aws_s3_bucket.services_backup_bucket.arn}"
  }
}

resource "aws_iam_policy" "services_backup_bucket_policy" {
  name   = "${var.env_name}_services_backup_bucket_policy"
  policy = "${data.template_file.services_backup_bucket_policy.rendered}"
}

resource "aws_iam_role_policy" "services_backup_bucket_access" {
  name   = "${var.env_name}_services_backup_bucket_access"
  role   = "${aws_iam_role.services_bucket_access.id}"
  policy = "${data.template_file.services_backup_bucket_policy.rendered}"
}

resource "aws_iam_user_policy_attachment" "services_backup_bucket_access" {
  user       = "${module.ops_manager.ops_manager_iam_user_name}"
  policy_arn = "${element(aws_iam_policy.services_backup_bucket_policy.*.arn, 0)}"
}

resource "aws_iam_role" "services_bucket_access" {
  name = "${var.env_name}_services_bucket_access"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    }
  ]
}
EOF
}