# VPN-Z1-01 ENI

resource "aws_network_interface" "tso-dev-eni-vpn-z1-01-perimeter" {
  subnet_id       = "${aws_subnet.tso-dev-subnet-z1-perimeter.id}"
  private_ips     = ["${var.tso-dev-eni-vpn-z1-01-perimeter-ip}"]
  security_groups = ["${aws_security_group.tso-dev-vpn-sg.id}"]

  source_dest_check = false

  attachment {
    instance     = "${aws_instance.tso-dev-vpn-z1-01.id}"
    device_index = 1
  }
}

resource "aws_network_interface" "tso-dev-eni-vpn-z1-01-private" {
  subnet_id         = "${aws_subnet.tso-dev-subnet-z1-private.id}"
  private_ips       = ["${var.tso-dev-eni-vpn-z1-01-private-ip}"]
  security_groups   = ["${aws_security_group.tso-dev-private-sg.id}"]
  source_dest_check = false

  attachment {
    instance     = "${aws_instance.tso-dev-vpn-z1-01.id}"
    device_index = 2
  }
}

###
### End of MCBOSS-Provided code
###
