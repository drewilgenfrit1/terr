# Perimeter route tables

resource "aws_route_table" "tso-dev-rt-z1-perimeter" {
  vpc_id = "${module.infra.vpc_id}"

  tags {
    Name = "tso-dev-rt-z1-perimeter"
  }
}

# Private route tables

resource "aws_route_table" "tso-dev-rt-z1-private" {
  vpc_id = "${module.infra.vpc_id}"

  tags {
    Name = "tso-dev-rt-z1-private"
  }
}

# Management route tables

resource "aws_route_table" "tso-dev-rt-z1-mgt" {
  vpc_id = "${module.infra.vpc_id}"

  tags {
    Name = "tso-dev-rt-z1-mgt"
  }
}

# Perimeter route table subnet association

resource "aws_route_table_association" "tso-dev-rt-z1-perimeter" {
  subnet_id      = "${aws_subnet.tso-dev-subnet-z1-perimeter.id}"
  route_table_id = "${aws_route_table.tso-dev-rt-z1-perimeter.id}"
}

# Private route table subnet association

resource "aws_route_table_association" "tso-dev-rt-z1-private" {
  subnet_id      = "${aws_subnet.tso-dev-subnet-z1-private.id}"
  route_table_id = "${aws_route_table.tso-dev-rt-z1-private.id}"
}

# MGT route table subnet association

resource "aws_route_table_association" "tso-dev-rt-z1-mgt" {
  subnet_id      = "${aws_subnet.tso-dev-subnet-z1-mgt.id}"
  route_table_id = "${aws_route_table.tso-dev-rt-z1-mgt.id}"
}

# MGT route table rules

resource "aws_route" "tso-dev-rt-z1-mgt-vdss" {
 count                     = "${var.pilot_vdss_pcx == ""? 0 : 1}"
 route_table_id            = "${aws_route_table.tso-dev-rt-z1-mgt.id}"
 destination_cidr_block    = "${var.vdss-mgt-ip}"
 vpc_peering_connection_id = "${var.pilot_vdss_pcx}"
}

# Perimeter route table rules

resource "aws_route" "tso-dev-rt-perimeter" {
 count                     = "${var.pilot_vdss_pcx == ""? 0 : 1}"
 route_table_id            = "${aws_route_table.tso-dev-rt-z1-perimeter.id}"
 destination_cidr_block    = "${var.vdss-vpn-ip}"
 vpc_peering_connection_id = "${var.pilot_vdss_pcx}"
}

# Perimeter route table rules

resource "aws_route" "tso-dev-rt-private" {
  route_table_id         = "${aws_route_table.tso-dev-rt-z1-private.id}"
  destination_cidr_block = "0.0.0.0/0"
  network_interface_id   = "${aws_network_interface.tso-dev-eni-vpn-z1-01-private.id}"
}

###
### End of MCBOSS-Provided code
###
