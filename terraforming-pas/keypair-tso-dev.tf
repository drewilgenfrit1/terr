resource "aws_key_pair" "tso_dev" {
  key_name   = "${var.env_name}-tso-dev-key"
  public_key = "${tls_private_key.tso_dev_private_key.public_key_openssh}"
}

resource "tls_private_key" "tso_dev_private_key" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}
