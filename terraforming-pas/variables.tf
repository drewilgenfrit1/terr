variable "hosted_zone" {
  default = ""
}

variable "vpc_cidr" {
  type    = "string"
  default = "10.0.0.0/16"
}

/******
* PAS *
*******/

variable "internetless" {
  default = false
}

variable "create_versioned_pas_buckets" {
  default = false
}

variable "create_backup_pas_buckets" {
  default = false
}

/****************
* Ops Manager *
*****************/

variable "ops_manager_ami" {
  default = ""
}

variable "optional_ops_manager_ami" {
  default = ""
}

variable "ops_manager_instance_type" {
  default = "r4.large"
}

variable "ops_manager_private" {
  default     = false
  description = "If true, the Ops Manager will be colocated with the BOSH director on the infrastructure subnet instead of on the public subnet"
}

variable "ops_manager_vm" {
  default = true
}

variable "optional_ops_manager" {
  default = false
}

/******
* RDS *
*******/

variable "rds_db_username" {
  default = "admin"
}

variable "rds_instance_class" {
  default = "db.m4.large"
}

variable "rds_instance_count" {
  type    = "string"
  default = 0
}

/*******************
* SSL Certificates *
********************/

variable "ssl_cert" {
  type        = "string"
  description = "the contents of an SSL certificate to be used by the LB, optional if `ssl_ca_cert` is provided"
  default     = ""
}

variable "ssl_private_key" {
  type        = "string"
  description = "the contents of an SSL private key to be used by the LB, optional if `ssl_ca_cert` is provided"
  default     = ""
}

variable "ssl_ca_cert" {
  type        = "string"
  description = "the contents of a CA public key to be used to sign the generated LB certificate, optional if or `ssl_cert` is provided"
  default     = ""
}

variable "ssl_ca_private_key" {
  type        = "string"
  description = "the contents of a CA private key to be used to sign the generated LB certificate, optional if or `ssl_cert` is provided"
  default     = ""
}

/*****************************
 * Isolation Segment Options *
 *****************************/

variable "isoseg_ssl_cert" {
  type        = "string"
  description = "the contents of an SSL certificate to be used by the LB, optional if `isoseg_ssl_ca_cert` is provided"
  default     = ""
}

variable "isoseg_ssl_private_key" {
  type        = "string"
  description = "the contents of an SSL private key to be used by the LB, optional if `isoseg_ssl_ca_cert` is provided"
  default     = ""
}

variable "isoseg_ssl_ca_cert" {
  type        = "string"
  description = "the contents of a CA public key to be used to sign the generated iso seg LB certificate, optional if `isoseg_ssl_cert` is provided"
  default     = ""
}

variable "isoseg_ssl_ca_private_key" {
  type        = "string"
  description = "the contents of a CA private key to be used to sign the generated iso seg LB certificate, optional if `isoseg_ssl_cert` is provided"
  default     = ""
}

/********
* Tags *
*********/

variable "tags" {
  type        = "map"
  default     = {}
  description = "Key/value tags to assign to all AWS resources"
}

/**************
* Deprecated *
***************/

variable "create_isoseg_resources" {
  type        = "string"
  default     = "0"
  description = "Optionally create a LB and DNS entries for a single isolation segment. Valid values are 0 or 1."
}

#########################################################################
# VPC region

# Key pairs

#variable "tso_dev_aws_key_name" {
#  description = "Name of the key pair for host access"
#  default     = "tso_dev"
#}

# Pilot VDSS VPC peering ID

variable "pilot_vdss_pcx" {
 description = "Name of the Pilot VDSS peering connection"
 default     = ""
}

# Availability zone variables

variable "availability_zone_1" {
  description = "Name of zone 1"
  default     = "us-gov-west-1c"
}

variable "availability_zone_2" {
  description = "Name of zone 2"
  default     = "us-gov-west-1b"
}

variable "availability_zone_3" {
  description = "Name of zone 3"
  default     = "us-gov-west-1a"
}

# Default Juniper vSRX AMI for us-gov-west-1

variable "vsrx_default_ami" {
  default = "ami-a26e01c3"
}

# Default RHEL 7 base AMI for us-gov-west-1

variable "linux_default_ami" {
  default = "ami-0466e865"
}

# Default Server 2016 base AMI for us-gov-west-1

variable "windows_default_ami" {
  default = "ami-d761f6b6"
}

# Default Server 2012R2 base AMI for us-gov-west-1

variable "windows2012r2_default_ami" {
  default = "ami-1329ba72"
}

# VPC and Subnet variables

variable "tso-dev-cidr" {
  description = "IP CIDR block for the Pilot VDMS VPC"
  default     = "10.0.16.0/22"
}

variable "tso-dev-z1-perimeter-cidr" {
  description = "IP CIDR block for tso-dev-z1-perimeter"
  default     = "10.0.16.0/27"
}

variable "tso-dev-z1-private-cidr" {
  description = "IP CIDR block for tso-dev-z1-private"
  default     = "10.0.16.32/27"
}

variable "tso-dev-z1-mgt-cidr" {
  description = "IP CIDR block for tso-dev-z1-mgt"
  default     = "10.0.16.64/27"
}

# Instance name variables

variable "vpn-z1-01_name" {
  type    = "string"
  default = "tso-dev-vpn-z1-01"
}

# Instance IP address variables

variable "tso-dev-eni-vpn-z1-01-mgt-ip" {
  description = "IP for the vpn-z1-01 MGT ENI"
  type        = "string"
  default     = "10.0.16.68"
}

variable "tso-dev-eni-vpn-z1-01-private-ip" {
  description = "IP for the vpn-z1-01 Private ENI"
  type        = "string"
  default     = "10.0.16.36"
}

variable "tso-dev-eni-vpn-z1-01-perimeter-ip" {
  description = "IP for the vpn-z1-01 Perimeter ENI"
  type        = "string"
  default     = "10.0.16.4"
}

# MGT instance IP variable

variable "vdss-mgt-ip" {
  description = "IP for the VDSS MGT ENI"
  type        = "string"
  default     = "10.0.8.121/32"
}

# VDSS VPN instance MGT IP variable

variable "vdss-vpn-ip" {
  description = "IP for the VDSS VPN ENI"
  type        = "string"
  default     = "10.0.8.68/32"
}
