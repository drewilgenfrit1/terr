#  Subnet tso-dev-subnet-z1-perimeter

resource "aws_subnet" "tso-dev-subnet-z1-perimeter" {
  vpc_id = "${module.infra.vpc_id}"

  cidr_block = "${var.tso-dev-z1-perimeter-cidr}"

  availability_zone = "${var.availability_zone_1}"

  tags {
    Name = "tso-dev-subnet-z1-perimeter"
  }
}

#  Subnet tso-dev-subnet-z1-private

resource "aws_subnet" "tso-dev-subnet-z1-private" {
  vpc_id = "${module.infra.vpc_id}"

  cidr_block = "${var.tso-dev-z1-private-cidr}"

  availability_zone = "${var.availability_zone_1}"

  tags {
    Name = "tso-dev-subnet-z1-private"
  }
}

#  Subnet tso-dev-subnet-z1-mgt

resource "aws_subnet" "tso-dev-subnet-z1-mgt" {
  vpc_id = "${module.infra.vpc_id}"

  cidr_block = "${var.tso-dev-z1-mgt-cidr}"

  availability_zone = "${var.availability_zone_1}"

  tags {
    Name = "tso-dev-subnet-z1-mgt"
  }
}

###
### End of MCBOSS-Provided code
###

#Note: "z1" subnet corresponds to the "1c" AZ instead "1a" out of pragmatism.
#Since the majority of AWS users will default to "1a, using "1c" ideally avoids
#resource constraints.
